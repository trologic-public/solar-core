export interface IShipmentSetting {
  enabled: boolean;
  title: string;
  minOrderTotal?: number;
  maxOrderTotal?: number;
  description?: string;
  region: IRegion[];
}

interface IRegion {
  country: string; // !!!!! Displayed as selection
  region?: string;
  baseFee: number | IBaseFee[]; // If no conditions, type is just a number
}

interface IBaseFee {
  baseOn: 'orderAmount' | 'weight';
  condition: IFeeRange[];
}

interface IFeeRange {
  min: number;
  max?: number;
  shippingFee: number;
}
