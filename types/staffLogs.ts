import { collections } from "../collections";

// adding a enum to reference what data will have logs
export interface IStaffLogAvailableType{
	collection: keyof typeof collections,
	enable: boolean
}
export const StaffLogAvailableData: IStaffLogAvailableType[] = Object.keys(collections).map(c => {
	const ignoreCollections: (keyof typeof collections)[] = [
		"staffLogs",
		"moduleSettings",
		"organizations"
	]
	return {
		collection: c,
		enable: !ignoreCollections.includes(c as keyof typeof collections)
	} as IStaffLogAvailableType
})

export interface IStaffLog {
	id: string;
	userId: string;
	action: StaffLogActionType;
	collection: StaffLogActionFilter,
	details: string;
	timestamp: Date;
}

export enum StaffLogActionType {
	Create = "create",
	Edit = "edit",
	Delete = "delete",
}

export type StaffLogActionFilter = keyof typeof collections;

export type IStaffLogFilters = {
	userId: string | "all";
	action: StaffLogActionType | "all";
	timeFrom: Date | null;
	timeTo: Date | null;
	details: keyof typeof collections | "all";
};

export type IStaffLogFilterValues = StaffLogActionFilter | string | StaffLogActionType | "all" | Date | null;