import { IGlobalSettings } from "./globalSettings";


export type T_AI_Settings = {
    disabled: boolean;
    sector: string;
    defaultKeywords: string[];
    culture: string;
    token: number;
    tokenUsage: T_AI_Token_Usages;
    defaultMonthlyToken: number;
    customModel?: T_AI_Custom_Model;
}

export type T_AI_Token_Usages = {
    textGenerate: number;
    imageGenerate: number;
    textImprove: number;
}

export type T_AI_Custom_Model = {
    enable: boolean;
    baseUrl: string;
    username?: string;
    password?: string;
    apiKey?: string;
    routes: Record<keyof T_AI_Token_Usages, string>;
}

export type T_AI_API_Payload = {
    aiSettings: T_AI_Settings;
    appSettings: IGlobalSettings;
    question: string;
    language: string;
    html?: boolean;
}

export const defaultAiSettings: T_AI_Settings = {
    disabled: true,
    sector: "",
    defaultKeywords: [],
    culture: "",
    token: 50,
    tokenUsage: {
        textGenerate: 1,
        imageGenerate: 2,
        textImprove: 4
    },
    defaultMonthlyToken: 50,
    customModel: {
        enable: false,
        baseUrl: "/api",
        username: "",
        password: "",
        apiKey: "",
        routes: {
            textGenerate: "/ai/text",
            imageGenerate: "ai/image",
            textImprove: "/ai/text/improve"
        }
    }
}