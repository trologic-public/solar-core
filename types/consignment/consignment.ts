import { T_Address } from "../general";
import { IProductItem } from "../product";


// Consignee: The party who sells the goods on behalf of the consignor.
export type T_Consignee = {
    id: string;
    name: string;
    createdAt: Date;
    updatedAt?: Date;
}

export type T_ConsigneeWithOutletCount = T_Consignee & {
    outletCount?: number;
}

export type T_Consignee_Outlet = {
    id: string;
    createdAt: Date;
    updatedAt?: Date;
    name: string;
    email: string;
    phone: string;
    address: T_Address;
    disabled: boolean;
}

export enum E_Consignee_Stock_Status {
    "Active"="Active",
    "Completed"="Completed"
}

export type T_Consignee_Stock = {
    id: string;
    /**
     * Auto suggest name of the stocking, based on consignee, outlet & product, can be manually changed
     */
    name: string;
    createdAt: Date;
    updatedAt?: Date;
    status: E_Consignee_Stock_Status;
    consignee: T_Consignee;
    outlet: T_Consignee_Outlet;
    /**
     * Product to be referenced from the system's products list, 
     * should only reference the product info and not the inventory or pricing
     */
    product: IProductItem;
    inventory: T_Consignee_Stock_Inventory;
    unitPrice: number;
    unitCommission: number;
    remarks?: string;
}

export type T_Consignee_Stock_Form = Omit<T_Consignee_Stock, 
    "outlet" | "product"
> & {
    outlet: T_Consignee_Outlet | null;
    product: IProductItem | null;
}

export type T_Consignee_Stock_Inventory = {
    currentStock: number;
    sold: number;
}

export enum E_Consignee_Invoice_Status {
    "Pending" = "Pending",
    "Issued" = "Issued",
    "Settled" = "Settled",
    "Voided" = "Voided",
}

export type T_Consignee_Invoice_Data = {
    createdAt: Date;
    status: E_Consignee_Invoice_Status;
    issueDate: Date | null;
    dueDate: Date;
    consignee: T_Consignee;
    items: T_Consignee_Invoice_Item[];
}

export type T_Consignee_Invoice = {
    id: string;
} & T_Consignee_Invoice_Data;

export type T_Consignee_Invoice_Item = {
    stock: T_Consignee_Stock;
    sold: number;
}