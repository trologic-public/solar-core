// ----------------------------------------------------------------------

import { IBooking } from "./booking";

export type ICalendarFilterValue = string[] | Date | null;

export type ICalendarFilters = {
	colors: string[];
	startDate: Date | null;
	endDate: Date | null;
};

// ----------------------------------------------------------------------

export type ICalendarDate = string | number;

export type ICalendarView = "dayGridMonth" | "timeGridWeek" | "timeGridDay" | "listWeek";
export type ICalendarViewTime = "timeGridWeek" | "timeGridDay";

export type ICalendarRange = {
	start: ICalendarDate;
	end: ICalendarDate;
} | null;

export interface ICalendarEvent {
	id: string;
	color: string;
	textColor?: string; // this one is background color, wtf...
	title: string;
	allDay: boolean;
	description: string;
	start: ICalendarDate | Date;
	end: ICalendarDate | Date;
	isMeeting: boolean;
	isDeadline?: boolean | null;
}

export function instanceOfIBooking(object: any): object is IBooking {
	return true;
}
