import { paths } from "../paths";
import { collections } from "../collections";

export type TCollections = typeof collections;
export type TPaths = typeof paths;

export interface IOrganization {
    name: string;
    tenantId: string;
    createdAt: Date;
    domain?: IOrgDomainInfo;
    customPaths?: DeepPartial<TPaths>;
}

export type DeepPartial<T> = {
    [P in keyof T]?: T[P] extends object ? DeepPartial<T[P]> : T[P];
};

export type TOrgVersion = {
    collections?: DeepPartial<TCollections>
    paths?: DeepPartial<TPaths>
    version: number
}

export interface IOrgDomainInfo {
    apexName: string
    createdAt: number
    customEnvironmentId?: null | string
    gitBranch?: null | string
    name: string
    projectId: string
    redirect?: null | string
    redirectStatusCode?: number
    updatedAt?: number
    verified: boolean
    verification?: any

    // custom prop for domain readiness
    ready?: boolean
}

export interface IOrgUserPayload {
    name: string
    email: string
    phone?: string
    password: string
}

export interface IOrgPayload {
    name: string
    displayName: string;
    migration: boolean;
    initialUser: IOrgUserPayload;
}