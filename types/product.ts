import { Accept } from "react-dropzone";
import { IAddressItem } from "./address";
import { ICategory } from "./category";
import { ICourier, ICourierTier } from "./courier";
import { IImage } from "./general";
import { IOrderLoyaltyPoint_Log } from "./loyalty-point";
import { IMembershipTier } from "./membership";
import { IPromotionItem } from "./promotion";

// ----------------------------------------------------------------------

export type IProductFilterValue = string | string[] | number | number[];

export type IProductFilters = {
	rating: string;
	gender: string[];
	category: string;
	colors: string[];
	priceRange: number[];
};

// ----------------------------------------------------------------------

export type IProductReviewNewForm = {
	rating: number | null;
	review: string;
	name: string;
	email: string;
};

export type IProductReview = {
	id: string;
	name: string;
	rating: number;
	comment: string;
	helpful: number;
	avatarUrl: string;
	isPurchased: boolean;
	attachments?: string[];
	postedAt: Date;
};

export enum EQnaType {
	"True or False" = "True or False",
	"Multiple Choice" = "Multiple Choice",
	"Short Answer" = "Short Answer",
	"File Upload" = "File Upload",
}

export type TQnaFileType = {
	name: string,
	accepts: Accept
}

// file type array
export const QnaFileTypes: TQnaFileType[] = [
	{
		name: "Images",
		accepts: {
			"image/png": [".png"],
			"image/jpg": [".jpg"],
			"image/jpeg": [".jpeg"],
		},
	},
	{
		name: "PDF",
		accepts: {
			"application/pdf": [".pdf"]
		},
	},
	{
		name: "Word Documents",
		accepts: {
			'application/msword': ['.doc'],
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document': ['.docx']
		}
	},
	{
		name: "Spreadsheets",
		accepts: {
			'text/csv': ['.csv'],
			'application/vnd.ms-excel': ['.xls'],
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['.xlsx'],
		}
	}
]

export type TQnaFileSettings = {
	maxFileSize?: number,
	fileTypes?: TQnaFileType[]
}

export interface IQNA {
	question: string;
	answer: string;
	yesNoQues?: boolean;
	type?: EQnaType,

	// Options are for MC questions
	options?: string[],

	// file settings only works with file type question
	fileSettings?: TQnaFileSettings
}

export type TQuesLang = "English" | "繁體中文" | "简体中文";

export interface IQuestionnaire {
	locale: TQuesLang;
	qna: IQNA[];
}

export interface IVat {
	enabled: boolean;
	percent: number;
}

export interface IProductBookingInfo {
	enabled: boolean;
	settings: IProductBookingSettings;
	useSlot?: boolean;
	slots?: IProductBookingSlot[];
	requireAddress?: boolean;
	haveVenue?: boolean;
	venueAddress?: string;
}

export type IProductItem = {
	id: string;
	sku: string;
	name: string;
	// need to inject all locale's name as an array
	names?: string[];
	code: string;
	price: number;
	taxes: number;
	tags: string[];
	gender: string;
	sizes: string[];
	publish: string;
	coverUrl: IImage;
	images: IImage[];
	colors: string[];
	quantity: number;
	// this old "category" property are no longer used
	category: string;
	// instead we will use this new property "categories"
	categories?: ICategory[];
	categoriesQuery?: string[];

	available: number;
	totalSold: number;
	description: string;
	totalRatings: number;
	totalReviews: number;
	inventoryType: string;
	subDescription: string;
	priceSale: number | null;
	vat?: IVat;
	reviews: IProductReview[];
	createdAt: Date;
	ratings: {
		name: string;
		starCount: number;
		reviewCount: number;
	}[];
	saleLabel: {
		enabled: boolean;
		content: string;
	};
	newLabel: {
		enabled: boolean;
		content: string;
	};
	booking: IProductBookingInfo;
	// nullable inventory records
	inventory?: IProductInventory;
	questions?: IQuestionnaire[];
	allowFile?: IAllowFile | null;

	// interfaces for delivery handling
	deliveryInfo?: IProductDeliveryInfo;

	// rules
	rules?: IProductRules;

	// toggle for point earning
	canEarnPoints?: boolean;

	// new feature!! Pre Order options
	preorder?: E_Product_Preorder_Options;
	preorderAllowPayLater?: boolean;
	estimatedReStockDate?: Date | null;
};

export enum E_Product_Preorder_Options{
	"disabled"="disabled",
	"admin-only"="admin-only",
	"full-preorder"="full-preorder"
}

export const Product_Preorder_Options: Record<keyof typeof E_Product_Preorder_Options, { label: string, icon: string, desc: string }> = {
	disabled: { label: "Disabled", icon: "", desc: "No pre-order is allowed for this product" },
	"admin-only": { label: "Admin Only", icon: "", desc: "Pre-order is available but only for admins to do so in dashboard" },
	"full-preorder": { label: "Full Pre-Order", icon: "", desc: "Pre-order is available for customers and admins" },
}

export interface IProductRules {
	minQty?: number;
}

// interfaces for delivery handling
export interface IProductDeliveryInfo {
	weight?: number;
	width?: number;
	height?: number;
	depth?: number;
}

export interface IAllowFile {
	hint: string;
	fileType: "file" | "image";
}

export interface IProductInventory {
	warehouseId: string;
	currentStock: number;
	maxStock: number;
	unitPerSlot: number;
}

export interface IProductBookingSettings {
	dayOfWeek?: number[];
	maxDay: number;
	openingTime?: string;
	closingTime?: string;
	duration: number;
	durationUnit: IProductBookingDurationUnit;
}

export const DayOfWeekLabelMap: { label: string, value: number}[] = [
	{ label: 'Monday', value: 1 },
	{ label: 'Tuesday', value: 2 },
	{ label: 'Wednesday', value: 3 },
	{ label: 'Thursday', value: 4 },
	{ label: 'Friday', value: 5 },
	{ label: 'Saturday', value: 6 },
	{ label: 'Sunday', value: 0 },
]

export enum IProductBookingDurationUnit {
	Minute = "minute",
	Hour = "hour",
	day = "day",
}

// slots interface for booking products
export enum IProductBookingSlotType {
	Recurring = "recurring",
	Single = "single",
}

export interface IProductBookingSlot {
	id: string;
	type: IProductBookingSlotType;
	// for all types
	name: string;
	from?: Date;
	to?: Date;
	date: Date;
	start: Date;
	end: Date;
	slots: number;
	// for recurring only
	dayOfWeek?: number[];
}

export const defaultBookingSlot: IProductBookingSlot = {
	id: "new",
	type: IProductBookingSlotType.Single,
	name: "",
	date: new Date(),
	start: new Date(),
	end: new Date(),
	slots: 1,
};

export const defaultBookingSettings: IProductBookingSettings = {
	dayOfWeek: [0, 1, 2, 3, 4, 5, 6],
	maxDay: 30,
	openingTime: "09:00",
	closingTime: `22:00`,
	duration: 1,
	durationUnit: IProductBookingDurationUnit.Hour,
};

export const defaultBookingInfoOn: IProductBookingInfo = {
	enabled: true,
	settings: defaultBookingSettings
}

export const defaultBookingInfoOff: IProductBookingInfo = {
	enabled: false,
	settings: defaultBookingSettings
}

export const defaultInventorySettings: IProductInventory = {
	warehouseId: "",
	currentStock: 0,
	maxStock: 50,
	unitPerSlot: 10,
};

export type IProductTableFilterValue = string | string[];

export type IProductTableFilters = {
	name: string;
	stock: string[];
	type: string[];
	publish: string[];
	category: ICategory[];
};

// ----------------------------------------------------------------------

export type ICheckoutCartItem = {
	id: string;
	name: string;
	sku?: string;
	coverUrl: IImage;
	available: number;
	price: number;
	colors: string[];
	size: string;
	quantity: number;
	subTotal: number;
	orderBooking?: ICheckoutOrderBooking | null | undefined;
	productBookingInfo?: IProductBookingInfo;
	deliveryInfo?: IProductDeliveryInfo;
	questions?: IQuestionnaire | null;
	rules?: IProductRules;
	vat?: IVat;
	product: IProductItem;
};

export const DefaultDeliveryInfo: IProductDeliveryInfo = {
	width: 0,
	height: 0,
	depth: 0,
	weight: 0,
};

export interface ICheckoutOrderBooking {
	date: Date;
	checkin: Date;
	checkout: Date;
	duration: number;
	durationUnit: IProductBookingDurationUnit;
}

export type ICheckoutDeliveryOption = {
	value: number;
	label: string;
	description: string;
};

export type ICheckoutPaymentOption = {
	value: string;
	label: string;
	description: string;
	disable?: boolean;
};

export type ICheckoutCardOption = {
	value: string;
	label: string;
};

export enum ICheckoutShippingOptions {
	Delivery = "Delivery",
	Collect = "Collect",
}

export type ICheckout = {
	total: number;
	subTotal: number;
	totalVat?: number;
	discount: number;
	shipping: number;
	activeStep: number;
	totalItems: number;
	cart: ICheckoutCartItem[];
	billing: IAddressItem | null;
	completedBooking: string | null;
	completedOrder: string | null;
	courier?: ICourier | null;
	courierTier?: ICourierTier | null;
	shippingAddress?: IAddressItem | null;
	shippingOption?: ICheckoutShippingOptions;
	questions?: IQuestionnaire | null;
	promotion?: IPromotionItem | null;
	membership?: IMembershipTier | null;

	// loyalty program related
	loyaltyProgram: IOrderLoyaltyPoint_Log
};

export interface IProductCat {
	id: string;
	name: string;
}

export const emptyQuestionnaire: IQuestionnaire = {
	locale: "English",
	qna: [{ question: "", answer: "" }],
};

export const defaultProductCat: IProductCat[] = [
	{
		id: "defaultProductCat-1",
		name: "Bookings",
	},
	{
		id: "defaultProductCat-2",
		name: "Consumables",
	},
];
