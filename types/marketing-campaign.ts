import { ETrackingSourceType } from "./tracking";

export enum E_MC_Dashboard_Tab{
    "general"="general",
    "targets"="targets",
    "ads-and-trackings"="ads-and-trackings",
    "templates"="templates",
    "mailing-list"="mailing-list",
    "runs"="runs",
}

export type TMarketingCampaign = {
    id: string;
    name: string;
    description: string;
    status: E_MC_Status;
    start?: Date;
    end?: Date;
    targets: T_MC_Target[];
    trackingSources?: T_MC_Target_Source[];
    emailTemplate?: string;
    landingPage?: any;
    defaultMailingList: T_MC_MailingItem[];
}
export enum E_MC_Status{
    "pending"="pending",
    "in-progress"="in-progress",
    "paused"="paused",
    "completed"="completed",
    "failed"="failed",
    "partial-failed"="partial-failed",
}
export const MC_Status_Color_Map: Record<keyof typeof E_MC_Status, string> = {
    "pending": "default",
    "in-progress": "info",
    "paused": "warning",
    "completed": "success",
    "failed": "error",
    "partial-failed": "warning"
}
export type T_MC_Email_Run = {
    id: string;
    campaignId: string;
    sentDate: Date;
    status: E_MC_Status;
    mailingList: T_MC_MailingItem[];
    sentList: T_MC_MailingItem[];
    failedList: T_MC_MailingItem[];
}
export type T_MC_MailingItem = {
    id: string;
    email: string;
    displayName: string;
    customerId?: string;
}
export enum E_MC_Target_Type {
    "Conversions" = "conversions",
    "Growth" = "growth",
    "Sales" = "sales"
}
export enum E_MC_Target_Unit {
    "Percentage" = "perc",
    "Amount" = "amount"
}
export enum E_MC_Target_Event_Type {
    "Order" = "order",
    "Booking" = "booking",
    "Signup" = "signup",
}
export type T_MC_Target_Source = {
    id: string;
    type: ETrackingSourceType;
    // depend on the source type, this id can be Ad id, Email template id etc
    trackingId?: string;
}
export type T_MC_Target = {
    id: string;
    title: string;
    description?: string;
    value: number;
    targetValue: number;
    type: E_MC_Target_Type;
    unit: E_MC_Target_Unit;
    event: E_MC_Target_Event_Type;
}