import { Dynamic_Data } from "../types/flex-fields";
import { IAddressItem } from "./address";
import { IGender } from "./customer";
import { ILoyaltyPoint_Wallet } from "./loyalty-point";
import { T_Magic_Link_Props } from "./magic-link";
import { ITag } from "./tag";

// ----------------------------------------------------------------------

export type IUserTableFilterValue = string | string[];

export type IUserTableFilters = {
  name: string;
  role: string[];
  status: string;
};

// ----------------------------------------------------------------------

export type IUserSocialLink = {
  facebook: string;
  instagram: string;
  linkedin: string;
  twitter: string;
};

export interface IUserPurchasedMembership {
  membershipId: string;
  purchasedAt: Date;
  validUntil: Date;
  cancelledAt?: Date;
}

export type IUserAccount = {
  id: string;
  uid?: string;
  email: string;
  emailVerified: boolean;
  isPublic: boolean;
  name?: string;
  displayName: string;
  phoneNumber: string | null;
  photoURL: string;
  shipmentDetails: IAddressItem[];
  createdAt: Date;
  updatedAt: Date;
  tenantId?: string | null | undefined;
  membershipLvl?: string;
  purchasedMemberships?: IUserPurchasedMembership[];
  tags?: ITag[];
  gender?: IGender;
  //
  spending?: number;

  // loyalty points handling
  loyaltyPointsWallet?: ILoyaltyPoint_Wallet;

  isAdmin?: boolean;
  /**
  * role is either system default roles id, or "custom", 
  * then which we should check for props customRole
  */
  userRole?: string;
  fa2Token?: string;
};

// for Admin app create org use
export type IAdminPayload = {
  email: string;
  isPublic: boolean;
  name?: string;
  displayName: string;
  phoneNumber: string | null;
  photoURL: string;
  shipmentDetails: IAddressItem[];
  createdAt: any;
  updatedAt: any;
  tenantId?: string | null | undefined;
  isAdmin?: boolean;
  role: string;
  uid: string;
  origin: string;
  gender?: IGender
};

export type T_Admin_Role_Info = {
  role: string;
  /**
  * role is either system default roles id, or "custom", 
  * then which we should check for props customRole
  */
  userRole?: string;
  jobTitle?: string;
  // custom fields
  customFields?: Dynamic_Data;
}

export type IAdmin = IUserAccount & T_Admin_Role_Info;

export type T_Admin_Invitation = T_Magic_Link_Props & {
  id: string;
  name: string;
  email: string;
  roleInfo: T_Admin_Role_Info;
};

export type T_Admin_Creation_Payload = T_Admin_Invitation & {
  password: string;
  phoneNumber: string;
  customFields: Dynamic_Data;
}

export type IUserAccountBillingHistory = {
  id: string;
  price: number;
  createdAt: Date;
  invoiceNumber: string;
};

export type IUserAccountChangePassword = {
  oldPassword: string;
  newPassword: string;
  confirmNewPassword: string;
};

export const defaultAdminRoles = [
  "Account",
  "Admin",
  "Assistant Manager",
  "Chief Executive Officer",
  "Chief Financial Officer",
  "Clerk",
  "Customer Service",
  "Director",
  "Executive Director",
  "Finance",
  "Human Resources",
  "Information Technology",
  "Manager",
  "Operation Manager",
  "Sales",
  "Sales & Marketing",
  "Senior Manager",
  "Vice President",
  "Other",
];

export interface ICurrentUser extends Partial<IUserAccount> {
  id: string;
  name: string;
  avatarUrl: string;
  isAdmin: boolean;
  spending: number;
  membershipLvl?: string;
  email: string;
}

export const emptyCurrentUser: ICurrentUser = {
  id: '',
  name: '',
  avatarUrl: '',
  isAdmin: false,
  spending: 0,
  membershipLvl: '',
  email: '',
};