export interface IPurchaseOrder {
	id: string;
	orderDate: Date;
	poNum: string;
	items: IPurchaseOrderItem[];
	supplier: ISupplier;
	totalAmount: number;
	status: IPurchaseOrderStatus;
	imported: boolean;
	updatedAt: Date;
	createdAt: Date;
	remarks: string;
	// You can include additional properties as needed
}

export interface IPurchaseOrderItem {
	productId: string;
	productName: string;
	quantity: number;
	unitPrice: number;
	total: number; // quantity * unitPrice
}

export interface ISupplier {
	supplierId: string;
	name: string;
	email: string;
	phone: string;
	// You can include additional supplier details
}

export enum IPurchaseOrderStatus {
	Draft = "Draft",
	Sent = "Sent",
	SentPaid = "Sent & Paid",
	ItemReceived = "Item Received",
	Canceled = "Cancelled",
	// Add more status options as needed
	Returned = "Returned"
}

export type IPurchaseOrderTableFilters = {
	poNum: string;
	supplier: string;
	status: IPurchaseOrderStatus | "all";
	imported: boolean | null;
	items: string;
};

export type IPurchaseOrderTableFilterValue = string | IPurchaseOrderStatus | "all" | boolean | null;
