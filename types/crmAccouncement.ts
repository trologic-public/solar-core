import { IImage } from './general';

export interface IAnnouncement {
  id: string;
  isDraft: boolean;
  title: string;
  shortDescription?: string;
  content: string;
  image?: IImage;
  tags: string[];
  updatedAt: Date;
}
