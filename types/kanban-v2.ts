import { MIMEType } from "util";
import { KA } from "./kanban-v2-automation"
import { IAdmin } from "./user";

export type IKanban2_Document = {
    id: string;
    board: IKanban2_Data_Board;
} 

export type IKanban2_Data<T> = IKanban2_Document & {
    columns: IKanban2_Data_Column[];
    cards: IKanban2_Data_Card<T>[];
    automations?: KA<T>[]
}

export type TKanban2View = "board" | "list";
export type IKanban2_Card_Display = "drawer" | "dialog";

export type IKanban2_Data_Board = {
    id: string;
    name: string;
    createdAt: Date;
    isDefault?: boolean;
    disabled?: boolean;
    canDelete?: boolean;
    readOnly?: boolean;
    cardDetailsDisplay?: IKanban2_Card_Display;
    enableAutomation?: boolean;
}
export type IKanban2_Data_Column = {
    id: string;
    isAfter: string | null;
    name: string;
    createdAt: Date;
    updatedAt?: Date | null;
}
export type IKanban2_Data_Card<T> = {
    id: string;
    createdAt: Date;
    updatedAt?: Date | null;
    archivedAt?: Date | null;
    trashedAt?: Date | null;
    deletedAt?: Date | null;
    columnId: string | null;
    isAfter: string | null;
    title: string;
    collaborators: string[]
    attachments?: IKanban2Attachment[]; 
    coverImage?: string | null;
    status?: EKanban2CardStatus;
    priority?: EKanban2CardPriority;
    comments: IKanban2Comment[]
    data?: T;
}

export type TKanban2History = {
    date: Date;
    admin: IAdmin;
    action: string;
}

export type IKanban2_Render<T> = {
    id: string;
    board: IKanban2_Data_Board;
    columns: IKanban2_Render_Column<T>[];
    archive: IKanban2_Data_Card<T>[]
    trashed: IKanban2_Data_Card<T>[]
}

export type IKanban2_Render_Column<T> = (IKanban2_Data_Column & {
    cards: IKanban2_Data_Card<T>[]
})

export enum EKanban2CardPriority{
    URGENT = "Urgent",
    HIGH = "High",
    MEDIUM = "Medium",
    LOW = "Low",
    VERY_LOW = "Very Low",
    NONE = "None"
}

export enum EKanban2CardStatus{
    TO_DO = "To Do",
    IN_PROGRESS = "In Progress",
    READY_TO_REVIEW = "Ready To Review",
    DONE = "Done"
}

export type IKanban2Comment = {
	id: string;
	name: string;
	message: string;
	avatarUrl: string;
	messageType: "image" | "text";
	createdAt: Date;
};

export interface IKanban2Attachment {
    id: string; // Unique identifier for the attachment
    name: string; // Original file name
    type: MIMEType | string; // Type of the attachment
    isImage: boolean;
    size: number; // Size of the file in bytes
    url: string; // URL to access the file
    uploadedAt: Date; // Timestamp when the file was uploaded
  }