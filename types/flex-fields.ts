export interface IImage {
    src: string;
    alt: string;
    width?: number;
    height?: number;
}

export type IFileBasic = {
    id: string;
    name: string;
    url: string;
  }

export type Field_Base = {
    name: string;
    index: number;
    display: Field_Display;
    settings: Field_Settings;
    options?: Field_Option[];
}

export type Field_Settings = {
    required?: boolean;
    canEdit?: boolean;
    prefix?: string;
    multiple?: boolean;
    minChar?: number;
    maxChar?: number;
    min?: number;
    max?: number;
    dateOnly?: boolean;
    allowPastDate?: boolean;
    allowFutureDate?: boolean;
}

export type Field_Display = {
    label: string;
    helper?: string;
}

export type Field_Option = {
    label: string;
    value: string;
}

export enum E_Field_Type_Names{
    "text"="text",
    "email"="email",
    "phone"="phone"
};

export type Field_Text = Field_Base & {
    type: "text",
    value: string;
    defaultValue?: string;
}

export type Field_Email = Field_Base & {
    type: "email",
    value: string;
    defaultValue?: string;
}

export type Field_Phone = Field_Base & {
    type: "phone",
    value: string;
    defaultValue?: string;
}

export type Field_Number = Field_Base & {
    type: "number",
    value: number;
    defaultValue?: number;
}

export type Field_Date = Field_Base & {
    type: "date",
    value: Date | null;
    defaultValue?: Date;
}

export type Field_Checkbox = Field_Base & {
    type: "checkbox",
    value: string[];
    defaultValue?: string[];
}

export type Field_Radio = Field_Base & {
    type: "radio",
    value: string;
    defaultValue?: string;
}

export type Field_Select = Field_Base & {
    type: "select",
    value: string[];
    defaultValue?: string[];
} 

export type Field_Data = (
    Field_Text | Field_Email | Field_Phone |
    Field_Number | Field_Date |
    Field_Checkbox | Field_Radio | Field_Select 
);

export type Field_Types = Omit<Field_Data, "value">;

export type Dynamic_Interface = Record<string, Field_Types>
export type Dynamic_Data = Record<string, Field_Data>;

export type Dynamic_Document = {
    id: string;
    createdAt: Date;
    updatedAt?: Date;
    data: Dynamic_Data
};

export type Dynamic_Collection = {
    name: string;
    path: string;
    interface: Dynamic_Interface
}

export type Field_Type_Names = Field_Data["type"];