import { IAdmin } from "./user"

// email settings
export interface IEmailNotificationSettings{
    triggers: {
        order: {
            new: boolean
            cancel: boolean
            refunded: boolean
        },
        booking: {
            new: boolean,
            cancel: boolean,
            refunded: boolean
        },
        customer: {
            new: boolean
        },
        newsletter: {
            newSubscription: boolean
        }
    },
    mailingList: IAdmin[]
}

export const defaultEmailNotificationSettings: IEmailNotificationSettings = {
    triggers: {
        order: {
            new: true,
            cancel: true,
            refunded: true,
        },
        booking: {
            new: true,
            cancel: true,
            refunded: true
        },
        customer: {
            new: true
        },
        newsletter: {
            newSubscription: true
        }
    },
    mailingList: []
}