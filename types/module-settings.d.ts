export interface IModuleSettingsData {
	path: string;
	hide: boolean;
	show?: boolean;
}

export interface IModuleSettings extends IModuleSettingsData {
	id: string;
}