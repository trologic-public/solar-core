export interface IWarehouseFields{
    name: string;
    enabled: boolean;
    visible: boolean;
    default: boolean;
    priority: number;
    address: string;
    slots: number;
}
export interface IWareHouse extends IWarehouseFields{
    id: string;
}