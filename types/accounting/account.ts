

export type T_Account = {
    id: string;
    name: string;
    description?: string;
    type: T_Account_Type;
    balance: number;
    isDefault?: boolean;

    // Bank AC info
    bankAccount?: T_Account_Bank_AC;

    // relationship
    relationship?: string;
}

export type T_Account_Bank_AC = {
    accountNumber: string; // Bank account number
    sortCode?: string; // Sort code or equivalent (e.g., IFSC, ABA, etc.)
    iban?: string; // International Bank Account Number
    swiftCode?: string; // SWIFT/BIC code
    accountName: string; // Account holder's name
    bankName: string; // Name of the bank
    currency: string; // Currency code (e.g., "USD", "GBP")
    balance: number; // Current account balance
    status: 'active' | 'inactive' | 'closed'; // Account status
    type: 'checking' | 'savings' | 'credit' | 'loan'; // Account type
    openingDate: Date; // Date when the account was opened
}

export const DefaultBankAc: T_Account_Bank_AC = {
    accountNumber: "012345678901",
    sortCode: "001234",
    swiftCode: "123456789",
    iban: "01234567890",
    accountName: "My bank account",
    bankName: "HSBC HK",
    currency: "HKD",
    balance: 0,
    status: "active",
    type: "savings",
    openingDate: new Date(),
}

export const BankAcKeys = Object.keys(DefaultBankAc) as (keyof T_Account_Bank_AC)[];


export type T_Account_Polarity = "credit" | "debit";

export type T_Account_Type = {
    id: keyof typeof AccountTypes_ID;
    name: string;
    polarity: T_Account_Polarity;
}

export const AccountTypes_ID = {
    "current-assets": "current-assets",
    "fixed-assets": "fixed-assets",
    "accumulated-depreciation": "accumulated-depreciation",
    "current-liabilities": "current-liabilities",
    "long-term-liabilities": "long-term-liabilities",
    "equity": "equity",
    "revenue": "revenue",
    "cogs": "cogs",
    "general-expenses": "general-expenses",
    "dividends": "dividends",
  } as const;

// Default values
export const AccountTypes: T_Account_Type[] = [
    // Normal Polarity (DR = up, CR = down)
    { id: 'current-assets', name: 'Current Assets', polarity: 'debit' },
    { id: 'fixed-assets', name: 'Fixed Assets', polarity: 'debit' },
    { id: 'cogs', name: 'Cost of Goods Sold', polarity: 'debit' },
    { id: 'general-expenses', name: 'General Expenses', polarity: 'debit' },
    { id: 'dividends', name: 'Dividends', polarity: 'debit' },

    // Reversed Polarity (DR = up, CR = down)
    { id: 'revenue', name: 'Revenue', polarity: 'credit' },
    { id: "accumulated-depreciation", name: "Accumulated Depreciation", polarity: 'credit' },
    { id: 'current-liabilities', name: 'Current Liabilities', polarity: 'credit' },
    { id: "long-term-liabilities", name: 'Long Term Liabilities', polarity: 'credit' },
    { id: 'equity', name: 'Equity', polarity: 'credit' },
]

export const AccountTypesObject: Record< string, T_Account_Type > = Object.fromEntries(AccountTypes.map(t => [t.id, t]));

// Array of default accounts with "default" prefix in both id and name
export enum E_Default_Account_Ids {
    // revenue
    'default-sales-revenue' = 'default-sales-revenue',
    // general expenses
    'default-general-expense' = 'default-general-expense',
    // cost of goods sold
    'default-cost-of-goods-sold' = 'default-cost-of-goods-sold',
    // current assets
    "default-cash" = "default-cash",
    'default-accounts-receivable' = 'default-accounts-receivable',
    // current liabilities
    'default-accounts-payable' = 'default-accounts-payable',
    'default-depreciation' = 'default-depreciation',
    'default-accrual' = 'default-accrual',
    // long term liabilities
    'default-long-term-liabilities' = 'default-long-term-liabilities',
    // fixed assets
    'default-fixed-assets' = 'default-fixed-assets',
    // accumlated depreciation
    'default-accumulated-depreciation' = 'default-accumulated-depreciation',
    // equity
    'default-issued-share-capital' = 'default-issued-share-capital',
    // dividends
    'default-dividends' = 'default-dividends',
}
export const defaultAccounts: Record<E_Default_Account_Ids, T_Account> = {
    'default-cash': {
        id: 'default-cash',
        name: 'Default Cash',
        type: AccountTypesObject[AccountTypes_ID["current-assets"]],
        balance: 0,
        isDefault: true
    },
    "default-accounts-receivable": {
        id: 'default-accounts-receivable',
        name: 'Default Accounts Receivable',
        type: AccountTypesObject[AccountTypes_ID["current-assets"]],
        balance: 0,
        isDefault: true
    },
    "default-accounts-payable": {
        id: 'default-accounts-payable',
        name: 'Default Accounts Payable',
        type: AccountTypesObject[AccountTypes_ID["current-liabilities"]],
        balance: 0,
        isDefault: true
    },
    "default-accrual": {
        id: 'default-accrual',
        name: 'Default Accrual',
        type: AccountTypesObject[AccountTypes_ID["current-liabilities"]],
        balance: 0,
        isDefault: true
    },
    "default-issued-share-capital": {
        id: "default-issued-share-capital",
        name: 'Default Issued Share Capital',
        type: AccountTypesObject[AccountTypes_ID["equity"]],
        balance: 0,
        isDefault: true
    },
    "default-sales-revenue": {
        id: 'default-sales-revenue',
        name: 'Default Sales Revenue',
        type: AccountTypesObject[AccountTypes_ID["revenue"]],
        balance: 0,
        isDefault: true
    },
    "default-general-expense": {
        id: 'default-general-expense',
        name: 'Default General Expense',
        type: AccountTypesObject[AccountTypes_ID["general-expenses"]],
        balance: 0,
        isDefault: true
    },
    "default-dividends": {
        id: 'default-dividends',
        name: 'Default Dividends',
        type: AccountTypesObject[AccountTypes_ID['dividends']],
        balance: 0,
        isDefault: true
    },
    "default-cost-of-goods-sold": {
        id: 'default-cost-of-goods-sold',
        name: 'Default Cost of Goods Sold',
        type: AccountTypesObject[AccountTypes_ID.cogs],
        balance: 0,
        isDefault: true
    },
    "default-fixed-assets": {
        id: 'default-fixed-assets',
        name: 'Default Fixed Assets',
        type: AccountTypesObject[AccountTypes_ID["fixed-assets"]],
        balance: 0,
        isDefault: true
    },
    'default-accumulated-depreciation': {
        id: 'default-accumulated-depreciation',
        name: 'Default Accumulated Depreciation',
        type: AccountTypesObject[AccountTypes_ID["accumulated-depreciation"]],
        balance: 0,
        isDefault: true,
        relationship: 'default-fixed-assets'
    },
    'default-depreciation': {
        id: 'default-depreciation',
        name: 'Default Depreciation',
        type: AccountTypesObject[AccountTypes_ID["general-expenses"]],
        balance: 0,
        isDefault: true,
    },
    'default-long-term-liabilities': {
        id: 'default-long-term-liabilities',
        name: 'Default Long Term Liabilities',
        type: AccountTypesObject[AccountTypes_ID["long-term-liabilities"]],
        balance: 0,
        isDefault: true
    },
}