import { E_Default_Account_Ids } from "./account";


export type T_Accounting_Settings = {
    yearStart: string;
    domicile: string;
    pipelines: T_Accounting_Pipelines;
}

export enum E_Accounting_Pipelines {
    "order" = "order",
    "booking" = "booking",
    "invoice" = "invoice",
    "purchase" = "purchase",
    "consignmentInvoice" = "consignmentInvoice"
}

export enum E_Accounting_Pipeline_Actions {
    "issued" = "issued",
    "settled" = "settled",
    "cancelled" = "cancelled",
    "refunded" = "refunded"
}

export type T_Accounting_Pipelines = Record<E_Accounting_Pipelines, T_Accounting_PipelineActions>;

export type T_Accounting_PipelineActions = Record<E_Accounting_Pipeline_Actions, T_Accounting_Pipeline_Instruction_Preset[]>;

export type T_Accounting_Instruction = "credit" | "debit";

export type T_Accounting_Pipeline_Instruction_Preset = {
    accountId: E_Default_Account_Ids | string;
    instruction: T_Accounting_Instruction;
}

export const defaultAccountingPipelineActions_sales: T_Accounting_PipelineActions = {
    issued: [
        {
            accountId: E_Default_Account_Ids["default-accounts-receivable"],
            instruction: 'debit'
        },
        {
            accountId: E_Default_Account_Ids["default-sales-revenue"],
            instruction: "credit"
        },
    ],
    settled: [
        {
            accountId: E_Default_Account_Ids["default-cash"],
            instruction: "debit",
        },
        {
            accountId: E_Default_Account_Ids["default-accounts-receivable"],
            instruction: "credit",
        }
    ],
    cancelled: [
        {
            accountId: E_Default_Account_Ids["default-sales-revenue"],
            instruction: 'debit',
        },
        {
            accountId: E_Default_Account_Ids["default-accounts-receivable"],
            instruction: "credit"
        }
    ],
    refunded: [
        {
            accountId: E_Default_Account_Ids["default-sales-revenue"],
            instruction: 'debit'
        },
        {
            accountId: E_Default_Account_Ids["default-cash"],
            instruction: "credit",
        },
    ]
}

export const defaultAccountingPipelineActions_purchase: T_Accounting_PipelineActions = {
    issued: [
        {
            accountId: E_Default_Account_Ids["default-cost-of-goods-sold"],
            instruction: "debit"
        },
        {
            accountId: E_Default_Account_Ids["default-accounts-payable"],
            instruction: "credit",
        }
    ],
    settled: [
        {
            accountId: E_Default_Account_Ids["default-accounts-payable"],
            instruction: "debit",
        },
        {
            accountId: E_Default_Account_Ids["default-cash"],
            instruction: "credit"
        },
    ],
    cancelled: [
        {
            accountId: E_Default_Account_Ids["default-accounts-payable"],
            instruction: 'debit',
        },
        {
            accountId: E_Default_Account_Ids["default-cost-of-goods-sold"],
            instruction: "credit"
        },
    ],
    refunded: [
        {
            accountId: E_Default_Account_Ids["default-cash"],
            instruction: "debit"
        },
        {
            accountId: E_Default_Account_Ids["default-cost-of-goods-sold"],
            instruction: "credit"
        },
    ]
}

export const defaultAccountingSettings: T_Accounting_Settings = {
    yearStart: "04-01",
    domicile: "Hong Kong",
    pipelines: {
        order: defaultAccountingPipelineActions_sales,
        booking: defaultAccountingPipelineActions_sales,
        invoice: defaultAccountingPipelineActions_sales,
        purchase: defaultAccountingPipelineActions_purchase,
        consignmentInvoice: defaultAccountingPipelineActions_sales
    }
}

export const months = [
    { label: "January", value: "01" },
    { label: "February", value: "02" },
    { label: "March", value: "03" },
    { label: "April", value: "04" },
    { label: "May", value: "05" },
    { label: "June", value: "06" },
    { label: "July", value: "07" },
    { label: "August", value: "08" },
    { label: "September", value: "09" },
    { label: "October", value: "10" },
    { label: "November", value: "11" },
    { label: "December", value: "12" },
];