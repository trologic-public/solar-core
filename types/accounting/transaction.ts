import { IBookingCustomer } from "../booking";
import { T_Consignee } from "../consignment/consignment";
import { ICustomer } from "../customer";
import { IFileBasic } from "../file";
import { IOrderCustomer } from "../order";
import { ISupplier } from "../purchaseOrder";
import { IAdmin } from "../user";
import { T_Account } from "./account";
import { E_Accounting_Pipelines, T_Accounting_Instruction } from "./accounting-settings";

export type T_Accounting_Transaction = {
    id: string;
    tenantId: string;
    createdAt: Date;
    isAutomated?: boolean;
    createdBy?: IAdmin | "system" | null;
    transactionDate: Date;
    account: T_Account;
    instruction: T_Accounting_Instruction;
    amount: number;
    reference: T_Accounting_Transaction_Reference;
    recipient: ICustomer | ISupplier | T_Accounting_Transaction_External_Recipient | IOrderCustomer | IBookingCustomer | T_Consignee;
    recipientType: E_Accounting_Transaction_Recipient_Type;
    supportings: IFileBasic[];
}

export enum E_Accounting_External_Type{
    "external"="external"
}

export const TransactionReferenceTypes = {
    ...E_Accounting_Pipelines,
    ...E_Accounting_External_Type
}

export type T_Accounting_Types = typeof TransactionReferenceTypes[keyof typeof TransactionReferenceTypes];

export type T_Accounting_Transaction_Reference = {
    id: string;
    name: string;
    type: T_Accounting_Types;
}

export enum E_Accounting_Transaction_Recipient_Type{
    "customer"="customer",
    "supplier"="supplier",
    "external"="external"
}

export type T_Accounting_Transaction_External_Recipient = {
    id: string;
    displayName: string;
    remark: string;
    relationship: string;
}

export const newRecipientTemplate: T_Accounting_Transaction_External_Recipient = {
    id: "new",
    displayName: "",
    relationship: "",
    remark: ""
}