import { UnitType, QUnitType } from "dayjs/index";

export enum E_Finance_Report_Types {
    "Profit And Loss" = "Profit And Loss",
    "Balance Sheet" = "Balance Sheet"
}

export type T_Finance_Report_Unit_Type = UnitType | QUnitType;

export const UnitOptions: readonly {
    label: string, value: T_Finance_Report_Unit_Type
}[] = [
    { label: "Month", value: "month" },
    { label: "Quarter", value: "quarter" },
    { label: "Year", value: "year" }
] as const;

export type T_Finance_Report_Form = {
    from: string;
    to: string;
    reportType: E_Finance_Report_Types;
    unit: T_Finance_Report_Unit_Type;
    emails?: string[];
}