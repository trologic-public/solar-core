import { collections } from "../../collections";
import { ICurrentUser, emptyCurrentUser } from "../user";

export type T_Finance_Log_Type = "account" | "transaction" | "report";
export type T_Finance_Log_Action = "create" | "update" | "delete" | "download";

export type T_Finance_Log = {
    tid: string;
    type: T_Finance_Log_Type;
	action: T_Finance_Log_Action | string;
	info?: string;
    date: Date;
    collection?: keyof typeof collections;
    id?: string;
    user?: ICurrentUser;
}

export const DefaultLog: T_Finance_Log = {
    tid: "",
    type: "account",
    action: "create",
    info: "",
    date: new Date(),
    collection: undefined,
    id: "",
    user: emptyCurrentUser
}