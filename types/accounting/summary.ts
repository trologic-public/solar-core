
export type AccountId = string;
export type MonthKey = string; // YYYYMM
export type TenantId = string;

export type T_Finance_Account_Summary = {
    credit: number;
    debit: number;
}
export type T_Finance_Account_Summary_Record = Record<AccountId, T_Finance_Account_Summary>;
export type T_Finance_Date_Summary = { date: Date, chartOfAccounts: T_Finance_Account_Summary_Record };
export type T_Finance_Date_Summary_Record = Record<MonthKey, T_Finance_Date_Summary>;
export type T_Finance_Tenanct_Summary_Record = Record<TenantId, T_Finance_Date_Summary_Record>;

/**
 * Monthly Summary Model
 *  "tenantId": {
 *      "month": {
 *          date: Date;
 *          chartOfAccounts: {
 *              "accountId": {
 *                  credit: 1234,
 *                  debit: 5678
 *              }     
 *          }
 *      }
 *  }
 */

export type T_Finance_Retain_Earnings = {
    date: Date;
    netProfitTotal: number; // net profit of the year
    prevRetainEarnings: number; // accumulated retain earnings from last year
}