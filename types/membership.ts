import { IDiscountUnit } from "./promotion";

export interface CashDollarRedemption {
	points: number;
	dollar: number;
	maxPerc: number;
}

export interface PointsPolicy {
	dollar: number;
	points: number;
}

export interface MembershipUpgradeCondition {
	spending: number;
	singlePurchaseCondition?: number | null;
}

export interface MembershipAwards {
	birthdayPoints: number;
	renewal: number;
}

// ========== CUSTOM MEMBERSHIP ==========
export interface IMembershipTier {
	id: string;
	title: string;
	name: string;
	enable: boolean;
	upgradeType: IUpgradeType;
	futureDiscount: IDiscount; // e.g. 15% discount from now on
	validity: number; // membership available for how many month or year
	validityUnit: "month" | "year";
	payPrice: number;
	needSpendAmount: number;
	benefits: string[];
	order: number; // the indexing order of this membership on the list

	// new flag: allowCustomerPurchase
	allowCustomerPurchase?: boolean;
}

export interface IDiscount {
	amount: number;
	unit: IDiscountUnit;
}

export type IUpgradeType = "payToUpgrade" | "spendToUpgrade";

export const defaultSystemMembership: IMembershipTier = {
	id: "basic-member",
	benefits: [],
	enable: true,
	futureDiscount: {
		amount: 0,
		unit: "perc",
	},
	name: "Basic",
	needSpendAmount: 0,
	order: 1,
	payPrice: 0,
	title: "Tier 1",
	upgradeType: "spendToUpgrade",
	validity: 0,
	validityUnit: "year",
};
