// eslint-disable-next-line import/no-cycle
import { IVat } from "./product";

export interface ICourier extends ICourierData {
	id: string;
}

export interface ICourierData {
	name: string;
	description: string;
	trackingUrl?: string;
	active: boolean;
	tiers: ICourierTier[];
	logo?: string;
	estimatedDuration?: ICourierEstimatedDuration;
}

export interface ICourierEstimatedDuration {
	days: number;
	name: string;
}

export interface ICourierTier {
	name: string;
	rule: ICourierTierRule;
	price: number;
	canWaive?: boolean;
	waiverOrderAmount?: number;
	estimatedDuration?: ICourierEstimatedDuration;
	vat?: IVat;
}

export const DefaultEstimatedDurations: ICourierEstimatedDuration[] = [
	{
		days: 1,
		name: `Next day`,
	},
	{
		days: 3,
		name: `3 days`,
	},
	{
		days: 5,
		name: `5 - 7 days`,
	},
];

export interface ICourierTierRule {
	dimension: number;
	weight: number;
}

export const DefaultCourierTier: ICourierTier = {
	name: `Base Tier`,
	rule: { dimension: 1, weight: 0.1 },
	price: 1.29,
	canWaive: false,
	waiverOrderAmount: 0,
};

export const DefaultCourierData: ICourierData = {
	name: `Default - (Standard Domestic)`,
	description: `Standard delivery`,
	active: true,
	tiers: [DefaultCourierTier],
	estimatedDuration: { days: 3, name: `3 days` },
};
