// eslint-disable-next-line import/no-cycle
import { IContact } from "./crmContact";
import { IKanban, IKanbanTask } from "./kanban";
import { ITag } from "./tag";

export interface IFunnel {
	id: string;
	enabled: boolean;
	title: string;
	stage: string[]; // 'Lead', 'Prospect', 'Active', 'Complete'
	// workflow?: IWorkflow;
	trigger: ITrigger | null;
	// automations?: IAutomation[];
	createdBy: string;
	updatedAt: Date;
	kanban: IKanban;
}

export enum TriggerType {
	landingPage = "landingPage",
	signUpPage = "signUpPage",
	checkoutPage = "checkoutPage",
}

export interface ITrigger {
	name: string;
	srcId: string;
	triggerType: TriggerType;
	condition?: ICondition | null;
	automation?: IAutomation | null;
}

export interface ICondition {
	motherId: string;
	id: string;
	condition: string;
}

export interface IAutomation {
	motherId: string;
	displayName: string;
	tag: string;
}

export interface IFunnelRecord {
	// THIS SHOULD HAS IT'S OWN DOCUMENT or SUB-DOCUMENT
	stage: string;
	contact: IContact;
	task: IKanbanTask[];
	tag: (string | ITag)[];
}

export const mockFunnel: IFunnel[] = [
	{
		id: "13",
		enabled: true,
		title: "Campaign",
		stage: ["Lead", "Potential", "Completed"],
		trigger: null,
		createdBy: "Simon Wong",
		updatedAt: new Date("2023-06-30 17:21:10"),
		kanban: {
			columns: {
				"1-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b1": {
					id: "1-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b1",
					name: "Lead",
					taskIds: ["1-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b1", "2-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2", "3-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3"],
				},
				"2-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2": {
					id: "2-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2",
					name: "Potential",
					taskIds: ["4-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4", "5-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5"],
				},
				"3-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3": {
					id: "3-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3",
					name: "High Potential",
					taskIds: [],
				},
				"4-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4": {
					id: "4-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4",
					name: "Completed",
					taskIds: ["6-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b6"],
				},
			},
			ordered: [
				"1-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b1",
				"2-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2",
				"3-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3",
				"4-column-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4",
			],
			tasks: {
				"1-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b1": {
					assignee: [],
					attachments: [],
					comments: [
						{
							avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
							createdAt: new Date(1693180131825),
							id: "12345",
							message: "testing comments",
							messageType: "text",
							name: "Bryan",
						},
					],
					description: "Color #FF1323 x 2箱, Color #FF0278 x 4箱",
					due: [new Date("2023-08-17T23:00:00.000Z"), new Date("2023-08-24T23:00:00.000Z")],
					id: "1-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b1",
					labels: [],
					name: "指甲油入貨",
					priority: "low",
					reporter: {
						avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
						id: "NEncxBDkaYgnQ083INroqAkLn5N2",
						name: "Shinji Hirota",
					},
					status: "To Do",
				},
				"2-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2": {
					assignee: [],
					attachments: [],
					comments: [],
					description: "頭皮草藥 x 4箱 (Don't forget the receipt this time)",
					due: [null, null],
					id: "2-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2",
					labels: ["Technology"],
					name: "購入頭皮草藥",
					priority: "high",
					reporter: {
						avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
						id: "NEncxBDkaYgnQ083INroqAkLn5N2",
						name: "Shinji Hirota",
					},
					status: "To Do",
				},
				"3-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3": {
					assignee: [],
					attachments: [],
					comments: [],
					description: "Spare key for male toilet missing",
					due: [null, null],
					id: "3-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3",
					labels: ["Technology", "Marketing"],
					name: "配廁所鎖匙",
					priority: "medium",
					reporter: {
						avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
						id: "NEncxBDkaYgnQ083INroqAkLn5N2",
						name: "Shinji Hirota",
					},
					status: "To Do",
				},
				"4-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4": {
					assignee: [],
					attachments: [],
					comments: [],
					description: "Contact Markus Fung",
					due: [null, null],
					id: "4-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4",
					labels: ["Technology", "Marketing", "Design"],
					name: "宣傳單張 - 追designer",
					priority: "high",
					reporter: {
						avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
						id: "NEncxBDkaYgnQ083INroqAkLn5N2",
						name: "Shinji Hirota",
					},
					status: "In Progress",
				},
				"5-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5": {
					assignee: [],
					attachments: [],
					comments: [],
					description: "Setup Facebook page for future marketing & promotions",
					due: [null, null],
					id: "5-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5",
					labels: [],
					name: "Facebook page setup",
					priority: "medium",
					reporter: {
						avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
						id: "NEncxBDkaYgnQ083INroqAkLn5N2",
						name: "Shinji Hirota",
					},
					status: "In Progress",
				},
				"6-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b6": {
					assignee: [],
					attachments: [],
					comments: [],
					description: "所有workstations不可留有污積、地下吸塵",
					due: [new Date("2023-09-01T13:25:57.000Z"), new Date("2023-09-02T13:25:57.000Z")],
					id: "6-task-e99f09a7-dd88-49d5-b1c8-1daf80c2d7b6",
					labels: ["Technology", "Marketing", "Design", "Photography", "Art"],
					name: "檢查及清理workstations",
					priority: "medium",
					reporter: {
						avatarUrl: "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_12.jpg",
						id: "NEncxBDkaYgnQ083INroqAkLn5N2",
						name: "Shinji Hirota",
					},
					status: "Done",
				},
			},
		},
	},
	{
		id: "8",
		enabled: false,
		title: "Promotion",
		stage: ["Lead", "Potential", "Completed"],
		trigger: null,
		createdBy: "Simon Wong",
		updatedAt: new Date("2023-06-23 15:43:28"),
		kanban: {
			columns: {},
			ordered: [],
			tasks: {},
		},
	},
];

export const emptyFunnel: IFunnel = {
	id: "new",
	enabled: true,
	title: "",
	stage: ["Lead", "Potential", "High Potential", "Completed"],
	trigger: null,
	createdBy: "",
	updatedAt: new Date(),
	kanban: { columns: {}, tasks: {}, ordered: [] },
};

export const defaultTriggers: ITrigger[] = [
	{
		name: "Signup Page",
		srcId: "trigger-signup-page",
		triggerType: TriggerType.signUpPage,
		condition: null,
		automation: null,
	},
	{
		name: "Checkout Page",
		srcId: "trigger-checkout-page",
		triggerType: TriggerType.checkoutPage,
		condition: null,
		automation: null,
	},
];

export const defaultConditions: ICondition[] = [
	{
		motherId: "trigger-signup-page",
		id: "condition-upon-sign-up-account",
		condition: "Upon sign up account",
	},

	{
		motherId: "trigger-checkout-page",
		id: "when-user-purchase",
		condition: "When user purchase",
	},
];

export const defaultAutomations: IAutomation[] = [
	{
		motherId: "condition-upon-sign-up-account",
		displayName: "Add to sales funnel as Lead",
		tag: "",
	},
	{
		motherId: "when-user-purchase",
		displayName: "Add to sales funnel as completed",
		tag: "",
	},
];