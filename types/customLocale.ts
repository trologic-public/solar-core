

export interface ICustomLocale{
    id: string; // this is the locale value e.g. en, zh, cn
    jsonString: string;
}

export const defaultCustomLocales: ICustomLocale[] = [
    {
        id: "en",
        jsonString: `
{
    "foo": "Foo",
    "bar": "Bar"
}
`
    }
]