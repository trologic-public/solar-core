export interface IPaymentSettings {
  bbmsl?: IBbmsl;
  bankTrsf?: IBankTrsf;
  cashOnDeli?: IPaymentBase;
  eftPay?: IEftPay;
  fastPaymentSys?: IFastPaySys;
  payDollar?: IPayDollar;
  paypal?: IPayPal;
  stripe?: IStripe;
}

interface IPaymentBase {
  enabled: boolean;
  minOrderTotal: number;
  maxOrderTotal: number;
  name: string;
  description: string;
}

interface IBbmsl extends IPaymentBase {
  merchantId: string;
  publicKey: string;
  key: string;
  gatewayUrl: string;
}

interface IBankTrsf extends IPaymentBase {
  receiptRequired: boolean;
}

interface IEftPay extends IPaymentBase {
  gatewayUrl: string;
  userConfirmKey: string;
  secretCode: string;
}

interface IFastPaySys extends IPaymentBase {
  receiptRequired: boolean;
}

interface IPayDollar extends IPaymentBase {
  merchantId: string;
  paymentType: string;
  gatewayUrl: string;
  gatewayLang: string;
  secureHashSecret: string;
}

interface IPayPal extends IPaymentBase {
  clientId: string;
  clientSecret: string;
  sandboxMode: boolean;
}

interface IStripe extends IPaymentBase {
  key: string;
  secret: string;
}
