

export type TTrackingStore = {
    tid: string; // tenant id
    uid?: string; // optional user id
    cid?: string; // optional campaign id
    srcType: ETrackingSourceType; // the source type gclid / fbclid etc
    srcId: string; // the source id
    startPath: string; // path that the conversion begin
    currentSales?: number; // keep track of the tracked session sales value
    date: string;
}

export enum ETrackingSourceType {
    "Google Ad" = 'google-ad',
    "FB Pixel" = 'fb-pixel',
    "Direct" = 'direct',
    "Email" = 'email',
    "Social Media Ads" = 'social-media-ads',
    "Search Engine Marketing" = "search-engine-marketing",
    "Others" = 'others'
}

export const TrackingSourceTypeKeys = Object.keys(ETrackingSourceType) as Array<keyof typeof ETrackingSourceType>;

export enum ETrackingEvents {
    "Landing" = 'landing',
    "Sign Up" = 'signup',
    "Checkout" = "checkout",
    "Order" = "order",
}

export type TTrackingValueType = 'count' | 'value';

export const TrackingEventsKeys = Object.keys(ETrackingEvents) as Array<keyof typeof ETrackingEvents>;

// types for tracking data reading
export type TTrackingDailyRecord = {
    date: Date;
    data: TTrackingSourceRecord;
}

export type TTrackingSourceRecord = {
    -readonly [K in keyof typeof ETrackingSourceType]: TTrackingEventRecord
}

export type TTrackingEventRecord = {
    -readonly [K in keyof typeof ETrackingEvents]: TTrackingData
}

export type TTrackingData = {
    uids: string[],
    count: number,
    salesValue?: number
}