import { TCrmMission } from "./crm-mission";
import { EKanban2CardStatus } from "./kanban-v2";

// KA - Kanban Automation
export interface KA<T> {
    id: string;
    event: KA_Event;
    action: KA_Action;
    lookupField: keyof T;
    updateStatus: EKanban2CardStatus | null | undefined;
    destinationStage: string | null | undefined;
    // addition field for campaign usage
    campaignId?: string | null | undefined;
    mission?: TCrmMission | null | undefined;
}

export type KA_Data<T> = Omit<KA<T>, 'id'>;

export enum KA_Event{
    "Customer Signup" = "Customer Signup",
    "Complete Order" = "Complete Order",
    "Complete Booking" = "Complete Booking",
    "Cancel Order" = "Cancel Order",
    "Cancel Booking" = "Cancel Booking",
}

export enum KA_Action{
    "Add Card" = "Add Card",
    "Move Card" = "Move Card",
    "Update Card Status" = "Update Card Status",
    "Delete Card" = "Delete Card",
    // additional status to use marketing campaign
    "Deliver Campaign" = "Deliver Campaign",
    // action to add mission to card
    "Assign Mission" = "Assign Mission"
}

// just a handy index map for events
export const Ka_Events_Index_Map = Object.keys(KA_Event).map((key, index) => ({ value: KA_Event[key as keyof typeof KA_Event], index}))