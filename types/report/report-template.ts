import { EBookingStatus } from "../booking";
import { IGender } from "../customer";
import { IOrderStatus } from "../order";
import { IProductItem } from "../product";
import { IPurchaseOrderStatus } from "../purchaseOrder";

export type T_Report_Base = { count: number }

export type T_Report_Monetary = T_Report_Base & {
    amount: number
}

export const Report_Template = {
    date: new Date,
    isMonthly: false,
    data: {
        order: {} as Record<IOrderStatus, T_Report_Monetary>,
        booking: {} as Record<EBookingStatus, T_Report_Monetary>,
        po: {} as Record<IPurchaseOrderStatus, T_Report_Monetary>,
        customer: {
            signup: {} as (
                T_Report_Base & {
                    gender: Record<IGender, T_Report_Base>,
                }
            ),
        },
        product: [] as (Pick<IProductItem, "id" | "name" | "coverUrl" | "sku"> & Record<IOrderStatus, T_Report_Monetary>)[],
        service: [] as (Pick<IProductItem, "id" | "name" | "coverUrl" | "sku"> & Record<EBookingStatus, T_Report_Monetary>)[],
    }
}

export type T_Report = typeof Report_Template;