export type IAddressItem = {
	id?: string;
	name: string;
	company?: string;
	primary?: boolean;
	fullAddress: string;
	phoneNumber?: string;
	addressType?: string;
};

export const emptyAddressItem: IAddressItem = {
	id: "",
	name: "",
	company: "",
	primary: false,
	fullAddress: "",
	phoneNumber: "",
	addressType: "",
};

//