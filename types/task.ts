export interface ITaskDetails{
    reporter: string;
    due: [Date | null, Date | null];
    description: string;
}