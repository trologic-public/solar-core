

export type T_Magic_Link_Props = {
    token: string;
    tenant_id: string;
    createdAt: Date;
    expiredAt: Date;
    revokedAt: Date | null;
    completedAt: Date | null;
}

export type T_Magic_Link_Verify_Result<T> = {
    verified: boolean;
    expired: boolean;
    revoked: boolean;
    completed: boolean;
    document: T | null;
}