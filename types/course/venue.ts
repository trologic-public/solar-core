

export type T_Venue = {
    id: string;
    tid: string;
    createdAt: Date;
    name: string;
    desc: string;
    enabled: boolean;
    maxPax: number;
    phone: string
    email: string;
    address: string;
}

export type T_Venue_Maintenance = {
    id: string;
    tid: string;
    venueId: string;
    createdAt: Date;
    from: Date;
    to: Date;
}