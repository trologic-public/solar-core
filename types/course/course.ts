import { ICustomer } from "../customer";
import { IImage } from "../general";
import { E_Payment_Status, T_Payment } from "../payment";
import { IAdmin } from "../user";
import { T_Venue } from "./venue";

export enum E_Course_Duration_Unit {
    'minute' = 'minute',
    'hour' = 'hour',
    'day' = 'day'
}

export type T_Course_Preset = {
    id: string;
    tid: string;
    createdAt: Date;
    disabled: boolean;
    name: string;
    courseCode: string;
    introduction: string;
    details: string;
    coverVideo?: string;
    coverImage?: IImage | null;
    images?: IImage[];
    duration: number;
    durationUnit: E_Course_Duration_Unit;
    price?: number;
    discount?: number;
    settings: {
        tutorRoleId?: string;
        minPax: number;
        maxPax: number;
        lastAdmission?: {
            value: number;
            unit: E_Course_Duration_Unit;
        };
        onCreate: {
            requiredVenue: boolean;
            requiredTutor: boolean;
        },
        onEnroll: {
            requiredConfirm: boolean;
        },

    }
}

export type T_Course = {
    id: string;
    createdAt: Date;
    tid: string;
    preset: T_Course_Preset;
    venue: T_Venue | null;
    tutor: IAdmin | null;
    start: Date;
    end: Date;
    cancelledAt: Date | null;
    published: boolean;
    enrollments?: T_Course_Enrollment[];
}

export type T_Course_Form = Omit<T_Course, 'preset' | 'end'> & {
    preset: T_Course_Preset | null;
    end: Date | null;
}

export enum E_Course_Enrollment_Status {
    "Pending" = "Pending",
    "Confirmed" = "Confirmed",
    "Cancelled" = "Cancelled",
    "Attended" = "Attended",
    "Missed" = "Missed"
}

export type T_Course_Enrollment = {
    id: string;
    tid: string;
    status: E_Course_Enrollment_Status;
    paymentStatus: E_Payment_Status;
    payments: T_Payment[];
    course: T_Course;
    attendee: ICustomer;
    enrolledAt: Date;
    cancelledAt: Date | null;
    refundedAt: Date | null;
    remarks: string;
    autoDeleteAt?: Date | null;
    requestedRefundAt?: Date | null;
}

export type T_Course_Enrollment_Form = Omit<T_Course_Enrollment, "attendee"> & {
    attendee: ICustomer | null;
}