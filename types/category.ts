
export enum ECategoryType{
    Product = "product",
    Service = "service",
    // Portfolio = "portfolio",
    // Post = "post"
}
export interface ICategory{
    id: string
    type: ECategoryType[]
    label: string
    icon?: string | null
    customIconSrc?: string
    slug: string
    description?: string
    parent?: ICategory | null | undefined
    ancestorIds?: string[]
    index?: number
}

export interface ICategoryList{
    category: ICategory
    path: string
    subCategories: ICategoryList[]
}

export interface ITypedCategoryList {
    type: ECategoryType
    categories: ICategoryList[]
}

export const DefaultNewCategory: ICategory = {
    id: 'new',
    icon: null,
    type: Object.values(ECategoryType),
    label: '',
    description: "",
    slug: '',
    index: 0
}