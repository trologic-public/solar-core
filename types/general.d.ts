import { DeepPartial } from ".";

// export type NestedKeys<T> = {
//     [K in keyof T]: T[K] extends object
//       ? K | `${K}.${NestedKeys<T[K]>}`
//       : K
//   }[keyof T]

type Join<K, P> = K extends string | number ? P extends string | number ? `${K}.${P}` : never : never;

export type NestedKeys<T> = {
  [K in keyof T]: T[K] extends object ? K | Join<K, NestedKeys<T[K]>> : K
}[keyof T];

// for global usage
export interface IImage {
    src: string;
    alt: string;
    width?: number;
    height?: number;
}

export interface T_Media {
    src: string;
    alt?: string;
    width?: number;
    height?: number;
    aspectRatio?: string;
    type?: "image" | "video"
}

export enum MaterialUIColorName {
    Primary = 'primary',
    Secondary = 'secondary',
    Error = 'error',
    Warning = 'warning',
    Info = 'info',
    Success = 'success',
}

export type Document<T> = T & {
    id: string;
}

export type TLocaleData<T> = {
		locale: string;
		data: T;
}

export type TDataWithLocale<T> = T & {
	locales?: TLocaleData<T>[];
}; 

export type TDeepPartialDataWithLocale<T> = T & {
	locales?: {
		locale: string;
		data: DeepPartial<T>;
	}[];
}; 

export type MIMEType =
| 'application/pdf'
| 'application/msword'
| 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
| 'image/jpeg'
| 'image/png'
| 'image/gif'
| 'text/plain'
| 'text/html'
| 'application/json'
| 'audio/mpeg'
| 'video/mp4'
| 'application/zip';

export type T_Address = {
    address1: string;
    address2?: string;
    city?: string;
    postcode?: string;
    country: string;
}