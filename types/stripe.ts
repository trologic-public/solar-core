export interface Stripe_Intent_Req_Payload{
    amount: number,
    currency: string,
    receipt_email?: string,
    description?: string
}