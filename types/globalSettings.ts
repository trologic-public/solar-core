import { IEmailNotificationSettings } from "./email-notifications";
import { TEncryptedData } from "./encryption";
import { IImage } from "./general";
import { IVat } from "./product";

export interface IContactInfo {
	name: string;
	email: string;
	phone: string;
	latlong: { lat: number; long: number };
	address1: string;
	address2: string;
	district: string;
	country: string;
	whatsappChat?: string;
	whatsappDefaultMessage?: string;
}

export interface IIntegrations {
	whatsappBusiness: any;
	sms: any;
	zoom: any;
	sendgrid: any;
	clickup: any;
}

export interface ISocialMediaSettings {
	facebook: string;
	instagram: string;
	linkedin: string;
	twitter: string;
}

export interface ISEOSettings {
	title: string;
	description: string;
}

export interface IWebsiteSettings {
	seo: ISEOSettings;
	socialMedia: ISocialMediaSettings;
	contact: IContactInfo;
	blogSettings: any; // coming soon
	shopSettings: any; // coming soon,
	customScripts: string;
	privacyContent: string;
	termsContent: string;
	botPenguinId: string;
	gaId?: string;
	facebookPixelId?: string;
	availableLocales?: string[];
	disableCustomerSite?: boolean;
}

export interface ICurrency {
	label: string;
	value: string;
}

// interfaces for delivery handling
export enum IDeliveryWeightUnit {
	KG = "Kg",
	Gram = "g",
}
export enum IDeliveryDimensionUnit {
	CM = "cm",
	Meter = "m",
	MM = "mm",
	Inch = "inch",
	Feet = "feet",
}

export interface ISystemPortal {
	allowRegistration: boolean;
	locales: any; // language, currency, timezone
	currency?: ICurrency;
	vat?: IVat;

	// interfaces for delivery handling
	deliveryUnits?: {
		weight: IDeliveryWeightUnit | null;
		dimension: IDeliveryDimensionUnit | null;
	};

	// email address for the sender of all outgoing emails in this system
	sender?: string;
	sgKey?: string | null;
	emailNotificationSettings?: IEmailNotificationSettings;

	// user roles show non access?
	hideReadOnlyAccess?: boolean;
}

export interface colorSet {
	base: string;
	contrast: string;
}
export interface IBrandingSettings {
	logo: IImage | null;
	favicon: IImage | null;
	splashscreenLogo: IImage | null;
	defaultCover: IImage | null;
	colors: {
		primary: colorSet;
		secondary: colorSet;
	};
}

export interface ICompanySettings {
	companyName: string;
	brandingSettings: IBrandingSettings;
}

export enum EPaymentOption {
	online = "Online Payment",
	pod = "Pay on Delivery",
}
export enum EDeliveryMethod {
	"Courier Delivery" = "courier",
	"Collect in Store" = "collect",
}
export interface IShopSettings {
	stripe?: {
		pub: string;
		// secret: string;
	};
	paypal?: {
		enabled: boolean;
		sandbox?: boolean;
		clientId: string;
		// secret: string;
	} | null;
	allowedPayments: {
		online: boolean;
		pod: boolean;
	};
	allowedDeliveries: {
		courier: boolean;
		collect: boolean;
	};
	minOrderAmount?: {
		amount: number;
		enabled: boolean;
	};
}

export type TEncryptedSecret = {
	stripe?: {
		pub: string;
		secret: TEncryptedData
	},
	paypal?: {
		enabled: boolean;
		sandbox?: boolean;
		clientId: string;
		secret: TEncryptedData
	} | null;
}

export type TShopSettingsWithEncryptedSecret = Omit<IShopSettings, "stripe" | "paypal"> & TEncryptedSecret;

export interface IGlobalSettings {
	company: ICompanySettings;
	website: IWebsiteSettings; // coming soon,
	integrations: IIntegrations;
	system: ISystemPortal;
	shop: IShopSettings;
}
