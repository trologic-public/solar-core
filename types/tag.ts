export type LabelColor =
  | 'default'
  | 'primary'
  | 'secondary'
  | 'info'
  | 'success'
  | 'warning'
  | 'error';

export interface ITag {
	id: string;
	tag: string;
	color: LabelColor;
	updatedAt?: Date;
}

