// ---- CUSTOMER BASIC ONLY ------- //
// ---- MISSING ORDER HISTORY & REWARD POINTS HISTORY ------- //

import { ITag } from "./tag";
import { ETrackingSourceType } from "./tracking";
import { IUserAccount } from "./user";

export interface ICustomer extends IUserAccount {
	enabled: boolean;
	isSubscribe: boolean;
	gender: IGender;
	assignedTo?: string; // !!!!! Displayed as selection in form
	description?: string;
	tags: ITag[];
	groups: string[]; // !!!!! Displayed as selection in form
	membershipLvl?: string;
	isAdmin: boolean;
	customerNumber: string;
	// adding spending for membership spend check
	spending?: number;

	// sim flag
	sim?: boolean;

	// conversion source when signup
	source?: ETrackingSourceType;

	signupAt?: Date;
}

export enum IGender {
	Male = "male",
	Female = "female",
	NotSpecified = "not specified",
}

export const getGender = (str: string) => {
	if (str.startsWith("m") || str.startsWith("M")) {
		return IGender.Male;
	}
	if (str.startsWith("f") || str.startsWith("F")) {
		return IGender.Female;
	}
	return IGender.NotSpecified;
};
