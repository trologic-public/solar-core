
export type T_DPage = {
    id: string;
    createdAt: Date;
    updatedAt?: Date;
    pubishedAt?: Date;
    status: E_DPage_Status;
    
}

export enum E_DPage_Status {
    "draft" = "Draft",
    "published" = "Published"
}