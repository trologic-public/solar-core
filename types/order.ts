import { IAddressItem } from "./address";
import { ICourier, ICourierTier } from "./courier";
import { IGender } from "./customer";
import { IOrderLoyaltyPoint_Log } from "./loyalty-point";
import { IPayment } from "./payment";
import { ICheckoutOrderBooking, IProductDeliveryInfo, IProductItem, IVat } from "./product";
import { IDiscountUnit, IPromotionItem } from "./promotion";
import { ITag } from "./tag";

export type IOrderTableFilterValue = string | Date | null;

export enum IOrderStatus {
	Pending = "pending",
	Completed = "completed",
	Cancelled = "cancelled",
	Refunded = "refunded",
}

export const itemUneditable = [IOrderStatus.Cancelled, IOrderStatus.Refunded];

export enum IDeliveryStatus {
	Processing = "processing",
	Partial = "partial delivered",
	Delivered = "delivered",
}

export type IOrderTableFilters = {
	name: string;
	status: string;
	startDate: Date | null;
	endDate: Date | null;
};

// ----------------------------------------------------------------------

export type IOrderHistory = {
	orderTime: Date;
	paymentTime: Date;
	deliveryTime: Date;
	completionTime: Date;
	timeline: {
		title: string;
		time: Date;
	}[];
};

export type IOrderShippingAddress = {
	fullAddress: string;
	phoneNumber: string;
	name?: string;
};

export type IOrderPayment = {
	cardType: string;
	cardNumber: string;
};

export type IOrderDelivery = {
	shipBy: string;
	speedy: string;
	trackingNumber: string;
	address: IOrderShippingAddress;
	courier?: ICourier;
	courierTier?: ICourierTier;
};

export type IOrderCustomer = {
	id: string;
	name: string;
	email: string;
	avatarUrl: string;
	ipAddress: string;
	gender: IGender;
	phone: string;
	tags?: ITag[];
	membershipLvl?: string;
	deliveryAddress?: IAddressItem | null;
};

export type IOrderProductItem = {
	id: string;
	sku: string;
	name: string;
	price: number;
	coverUrl: string;
	quantity: number;
	orderBooking?: ICheckoutOrderBooking | null | undefined;
	deliveryInfo?: IProductDeliveryInfo;
	vat?: IVat;
	product?: IProductItem
};

export type IOrderItem = {
	id: string;
	status: IOrderStatus;
	subTotal: number;
	orderNumber: string;
	history: IOrderHistory | null;
	customer: IOrderCustomer;
	delivery: IOrderDelivery | null;
	payments: IPayment[];
	items: IOrderProductItem[];
	discount: number;
	discountUnit: IDiscountUnit;
	taxes: number;
	shipping: number;
	totalAmount: number;
	totalQuantity: number;
	createdAt: Date;
	createdByStaff?: {
		id: string;
		name: string;
		avatarUrl: string;
	};
	remark?: string;
	promotion?: IPromotionItem | null;
	deliveryStatus?: IDeliveryStatus;
	courier?: ICourier | null;
	courierTier?: ICourierTier | null;
	restockedItems?: IOrderProductItem[];
	vat?: number;
	loyaltyPointLog?: IOrderLoyaltyPoint_Log;
	bankInSlip?: string;
	preorder?: boolean;
	preorderFulfilled?: boolean;
	fulfilledDate?: Date | null;

	// new state for admin to customise final total
	adminOffset?: number;
};

export interface StripePaymentDetails {
	paymentIntentId: string;
	paymentIntentClientSecret: string;
	paymentIntentStatus: string; // Requires Payment, Succeeded, Failed, etc.
	// You can add more Stripe-specific fields here as needed
}

export interface StripeEntities {
	paymentIntent: StripePaymentIntent;
	customer: StripeCustomer;
	paymentMethod: StripePaymentMethod;
	charge: StripeCharge;
	refund: StripeRefund;
	error: StripeError;
}

export interface StripePaymentIntent {
	id: string;
	amount: number;
	currency: string;
	status: string;
	// Other properties like payment method, customer, etc.
}

export interface StripeCustomer {
	id: string;
	email: string;
	// Other customer-related properties
}

export interface StripePaymentMethod {
	id: string;
	type: string; // 'card', 'bank_account', etc.
	// Other payment method properties
}

export interface StripeCharge {
	id: string;
	amount: number;
	currency: string;
	status: string;
	// Other charge-related properties
}

export interface StripeRefund {
	id: string;
	amount: number;
	currency: string;
	status: string;
	// Other refund-related properties
}

export interface StripeError {
	code: string;
	message: string;
	// Other error-related properties
}

// preorder handling types
export type T_Fulfill_Payload = {
	orders: IOrderItem[],
	notify: boolean,
	fromStock: boolean
}