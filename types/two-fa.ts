

export type T2FASession = {
    tid: string;
    uid: string;
    source: T2FASource;
    token: string;
    verifiedAt: Date;
    validUntil: Date;
}

export type T2FASource = "SMS" | "email";

