import { IGender } from "./customer";
import { ITag } from "./tag";

export interface IContact {
	id: string;
	basicInfo: BasicInfo;
	companyInfo?: CompanyInfo;
	tag: ITag[]; // !!!!! Displayed as selection
	group: string[]; // !!!!! Displayed as selection
	address?: IAddress;
	internalNote: string;
	avatarUrl: string;
	createdAt: Date;
}

interface BasicInfo {
	firstName: string;
	lastName: string;
	email: string;
	phone?: string;
	gender?: IGender;
}

interface CompanyInfo {
	company: string;
	dept: string;
	jobTitle: string;
	officeNum: string;
	fax: string;
	website: string;
}

interface IAddress {
	firstName: string;
	lastName: string;
	country: string;
	phone: string;
	address: string;
}
