

// shared types
export type TFX_Global_Property = {
    id: string;
    cid: string;
    name: string;
    hide: boolean;
    className: string;
    css: string;
}

// Text Components
export type TFX_Text_Comoponents = TFX_Text_Block | TFX_Heading;

export type TFX_Text_Block = TFX_Global_Property & {
    html: string;
}

export type HeadingTag = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';

export type TFX_Heading = TFX_Global_Property & {
    size: HeadingTag
    content: string
}

// Media Components
export type TFX_Media_Components = TFX_Image_Block | TFX_Video_Block;
export type TFX_Image_Block = TFX_Global_Property & {
    src: string;
    title: string;
    alt: string;
    caption?: string;
    width?: string;
    height?: string;
}
export type TFX_Video_Block = TFX_Global_Property & {
    type: "url" | "html";
    src: string;
    caption?: string;
    width?: string;
    height?: string;
}

// Layout Components
export type TFX_Layout_Components = TFX_Grid_Layout | TFX_5050_Layout;

export type TFX_Layout_Shared = TFX_Global_Property & {
    gap: number;
    items: any[];
    fluid: boolean;
}

export type TFX_Grid_Layout = TFX_Layout_Shared & {
    columns: number | "auto";
}
export type TFX_5050_Layout = TFX_Layout_Shared & {
    ratio: "33/66" | "25/75" | "50/50" | "66/33" | "75/25";
}

// Interactive Components
export type TFX_Interactive_Components = TFX_Accordions | TFX_Tabs | TFX_Slider;
export type TFX_Accordions = TFX_Global_Property & {}
export type TFX_Tabs = TFX_Global_Property & {}
export type TFX_Slider = TFX_Global_Property & {}

// List Components

// Media Gallery Components

// Form Components

// Content Blocks

// Navigation Components

// Data Display Components

// Advanced Components

// Custom Components

