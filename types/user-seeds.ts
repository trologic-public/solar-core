import { IGender } from "./customer";

export type T_User_Seed = {
    firstName: string;
    altFirstName?: string;
    lastName: string;
    email: string;
    gender: IGender;
    phone?: string;
    originProject: string;
    isStaff: boolean;
}

export type T_User_Seed_Doc = { id: string } & T_User_Seed;