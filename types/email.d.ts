import type { AttachmentData } from "@sendgrid/helpers/classes/attachment";
import { IBooking } from "../types/booking";
import { IOrderItem } from "./order";
import { ICustomer } from "./customer";

export interface IEmailSendPayload {
    recipients: string[];
    subject: string;
    html: string;
    attachments?: (AttachmentData & {content_id: string})[];
    booking?: IBooking;
}

export interface IBEBooking{
    booking: IBooking
}

export interface IBEOrder{
    order: IOrderItem
}

export interface IBECustomer{
    customer: ICustomer
}