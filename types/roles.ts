import { Dynamic_Interface } from "../types/flex-fields";
import { paths } from "../paths";
import { NestedKeys } from "../types";


export type IUserRole = {
    id: string;
    name: string;
    external: boolean;
    isDefault: boolean;
    systemDefault: boolean;
    allowEdit: boolean;
    allowDelete: boolean;
    hidden: boolean;
    defaultPermission: EUserPermissionLevel;
    permissions: IUserPermission[];
    customFields?: Dynamic_Interface;
}

export enum EUserPermissionLevel {
    "No Access" = 0,
    "Can Read" = 1,
    "Can Edit" = 2,
    "Super" = 3
}

export type IUserPermission = {
    scope: keyof typeof paths.dashboard;
    permission: EUserPermissionLevel
}

export type IPathPermission = {
    path: NestedKeys<typeof paths.dashboard>,
    permission: IUserPermission
}