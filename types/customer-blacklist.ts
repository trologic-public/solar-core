import { ICustomer } from "./customer";

export enum E_Customer_Blocking_Scope {
    "shop" = "shop",
    "full-website" = "full-website"
}

export type T_Customer_Blacklist_Item = {
    id: string;
    customer: ICustomer;
    enabled: boolean;
    blockScope: E_Customer_Blocking_Scope;
}

export type T_Customer_Blacklist_Item_State = Omit<T_Customer_Blacklist_Item, "customer"> & { customer: ICustomer | undefined };