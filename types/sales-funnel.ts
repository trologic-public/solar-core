import { TCrmMission } from "./crm-mission";

export interface ISalesFunnelDetails{
    reporter: TAutomatedReporter | string;
    due: [Date | null, Date | null];
    description: string;
    customerId: string;
    expectedRevenue?: number;
    customerExpectation?: string;
    missions?: TCrmMission[];
}

export type TAutomatedReporter = {
    isAutomatedUser: boolean;
    name: string;
    photoURL: string;
}

export const defaultAutomatedUser: TAutomatedReporter = {
    isAutomatedUser: true,
    name: "Automated",
    photoURL: ""
}