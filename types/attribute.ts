export interface IAttribute {
  id: string;
  isGlobal: boolean;
  position: string;
  key: string;
  label: string;
  updatedAt: Date;
}
