import { IAdmin } from "./user";

type FieldReview<T> = {
	[K in keyof T]: {
		verified: boolean;
		reason: string;
	}
};

export type TOnboardingSystemData = {
	id: string;
	uid: string;
	appliedAt: Date;
	updatedAt?: Date;
	closedAt?: Date;
}

export type TOnboardingContactDetails = {
	// step 1: contact info
	name: string;
	phone: string;
	email: string;
}

export type TOnboardingCompanyDetails = {
	// step 2: company information
	company: string;
	website: string;
	address1: string;
	address2?: string;
	br?: string;
	brDocument?: string;
}

export type TOnboardingAgreementData = {
	// step 3: Application Agreement
	signedAgreement: string;
	signature: string;
}

export type TOnboardingMessage = {
	// step 4: introduction and enquiry
	message?: string;
}

export type TOnboardingApplicationData =
	TOnboardingContactDetails &
	TOnboardingCompanyDetails &
	TOnboardingAgreementData;

export type TOnboardingFieldReview = FieldReview<TOnboardingApplicationData>;

// Admin Approval Data
export type TOnboardingApprovalData = {
	status: EOnboardingStatus;
	review: TOnboardingFieldReview;
	resultDetails: string;
	processingLogs: IOnboardingProcessingLog[]
}

export type TOnboarding =
	TOnboardingSystemData &
	TOnboardingApplicationData &
	TOnboardingMessage &
	TOnboardingApprovalData

export type TOnboardingApplicationForm = Omit<TOnboarding, 'brDocument' | 'signature' | 'signedAgreement'> & {
	brDocument: File | null;
	signedAgreement: string | null;
	signature: string | null;
}

export enum EOnboardingStatus {
	"Pending" = "Pending",
	"Processing" = "Processing",
	"Need Amendments" = "Need Amendments",
	"Approved" = "Approved",
	"Declined" = "Declined",
	/**
	 * Closed status requires customer end action to trigger.
	 * New application can only be apply when no existing applications, 
	 * or all existing applications are in "Closed" status
	 */
	"Closed" = "Closed"
}

export interface IOnboardingProcessingLog {
	admin: IAdmin;
	date: Date;
	fromStatus: EOnboardingStatus;
	toStatus: EOnboardingStatus;
}

export interface IOnboardingSettings {
	documentContent: string; // wysiwyg
}