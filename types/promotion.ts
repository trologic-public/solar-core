export type IDiscountUnit = "perc" | "amount";

export interface IPromotionItem {
	id: string;
	name: string;
	description: string;
	type: string;
	enabled: boolean;
	from: Date;
	to: Date;
	priority: number;
	discount: number;
	discountUnit: IDiscountUnit;
	useWithMembership?: boolean;
}

export type IPromotionTableFilters = {
	name: string;
};
