import { ETrackingEvents, TTrackingValueType } from "./tracking"

export type TCrmMission = {
    id: string
    name: string
    description: string
    createdAt: Date
    updatedAt?: Date
    dueDate?: Date
    targetType: ETrackingEvents
    valueType: TTrackingValueType
    targetValue: number
    currentValue: number
    targetId: string // document ID of the target (campaign / customer etc)
    disabled?: boolean
    completed: boolean
}

export const genNewMission = (id: string, targetId: string): TCrmMission => ({
    id,
    name: "",
    description: "",
    createdAt: new Date(),
    targetType: ETrackingEvents.Order,
    valueType: "count",
    targetValue: 10,
    currentValue: 0,
    targetId,
    disabled: false,
    completed: false
})