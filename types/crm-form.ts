import { IFileBasic } from "./file";

export enum ICRMFormQuestionType{
    Text = 'Text',
    Textarea = 'Textarea',
    Select = 'Select',
    Checkbox = 'Checkbox',
    Radio = 'Radio',
    FileUpload = 'File Upload',
    Phone = 'Phone',
    Email = 'Email'
}

export interface ICRMFormQuestionOption{
    label: string;
    value: string;
}

export enum ICRMFormQuestionFileType{
    Image = 'image',
    PDF = 'pdf',
    Excel = 'xls',
    Word = 'word',
}
export interface ICRMFormQuestion{
    type: ICRMFormQuestionType;
    question: string;
    options?: ICRMFormQuestionOption[];
    file?: IFileBasic;
    allowedFileTypes?: ICRMFormQuestionFileType[];
    value?: string | string[];
}
export interface ICRMFormTab{
    questions: ICRMFormQuestion[];
}
export interface ICRMFromSettings{
    slug: string;
    enable: boolean;
    allowGuest: boolean;
    singleSubmit: boolean;
    metaTitle: string;
    metaDescription: string;
}
export interface ICRMForm{
    id: string;
    name: string;
    description: string;
    tabs: ICRMFormTab[];
    settings: ICRMFromSettings;
}