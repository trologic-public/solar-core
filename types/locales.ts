import { Localization } from '@mui/material/locale';

export interface ILocale {
  label: string;
  value: string;
  icon: string;
}

export interface ILocaleData<T> {
  locale: ILocale;
  data: Partial<T>;
}

export interface ILocaleSys extends ILocale{
  systemValue: Localization;
}