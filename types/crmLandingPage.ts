export interface ILandingPage {
  id: string;
  name: string;
  url: string;
  isPublished: boolean; // Not sure about this
  createdAt: Date;
  updatedAt: Date;
  designContent: IDesignContent;
}

interface IDesignContent {
  // This is 1 of the 3 types of template
  header: {
    textColor: string;
    backgroundColor: string;
    align: 'flex-start' | 'center' | 'flex-end';
  };
  crmAuto: {
    title: string;
    subtitle: string;
    backgroundImg: string;
    textColor: string;
    align: 'flex-start' | 'center' | 'flex-end';
  };
  section1: {
    backgroundColor: string;
    textColor: string;
    title: string;
    content: string;
    image: string;
  };
  section2: {
    backgroundColor: string;
    textColor: string;
    title: string;
    content: string;
    image: string;
  };
  eBook: {
    title: string;
    form: IForm;
    textColor: string;
    backgroundColor: string;
    buttonColor: string;
  };
  footer: {
    fb: ISocialMedia;
    ig: ISocialMedia;
    twit: ISocialMedia;
    email: ISocialMedia;
    backgroundColor: string;
    btnColor: string;
  };
}

interface ISocialMedia {
  link: string;
  enabled: boolean;
}

interface IForm {
  id: string;
  name: string;
}
