import { IImage } from "./general";
import { ITag } from "./tag";

export interface IBroadcast {
	id: string;
	broadcastType: "email" | "sms";
	topic: string;
	content: string;
	image?: IImage;
	status: "pending" | "sent";
	whenSend: Date; // choose "NOW" or "specific date time"
	targetGroup: string[]; // select customer group and/or contact group
	updatedAt: Date;
	// shinji todo: customList should be list of emails
	customList?: string[];
	targetIndividual?: boolean;
	// V2 props for broadcasting groups and tags
	tags?: ITag[];
	broadcastGroup?: IBroadcastGroup[];
	videoId?: string;
	videoThumbnail?: string;
	hashTags?: string[];
}

export enum IBroadcastGroup { // If add more, need to add in translation files also
	Customer = "customers",
	Contact = "contacts",
}

export interface IBroadcastGroupUser{
	email: string;
	tags: ITag[];
}