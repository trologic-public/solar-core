export interface ISettings {
  basic: IBasic;
  contact?: IContact;
  image: IImage;
  product: IProduct;
  homepage: IHomepage;
  seo: ISEO;
  config: IConfig;
  email?: IEmail;
  smtpDefault: boolean;
  currency: ICurrency[];
  language: ILang;
  fbSetting?: IFBSetting;
  template: ITemplate;
  gRecap?: IGoogleRecap;
  commission: ICommission;
  azure?: IAzure;
  employee: IEmployee;
  clickUp?: IClickUp;
  whatsapp?: IWhatsapp;
  sms?: ISms;
  zoom?: IZoom;
}

interface IBasic {
  storeName: string;
  timeZone: string; // !!!!! Displayed as selection
  supportedCountries: string; // !!!!! Displayed as selection
  defaultAreaCode: string; // !!!!! Displayed as selection
  smsSupportedAreaCodes: string; // !!!!! Displayed as selection
  headerScript?: string;
  footerScript?: string;
}

interface IContact {
  name?: string;
  phone?: string;
  email?: string;
  district?: string;
  address1?: string;
  address2?: string;
  guide?: string;
}

interface IImage {
  logo?: string;
  icon?: string;
  defaultBanner?: string;
  adminLogo?: string;
}
interface IProduct {
  recentViewedProducts: boolean;
  relatedProducts: boolean;
}
interface IHomepage {
  hotProducts: boolean;
  newProducts: boolean;
  featuredProducts: boolean;
  blogArticle: boolean;
  customerColor: boolean;
  navigationBgColor: string;
  navigationTxtColor: string;
  iconColor: string;
  footerBgColor: string;
}

interface ISEO {
  metaTitle: string;
  description: string;
}
interface IConfig {
  catalogMode: boolean;
  memberOnly: boolean;
  guestCheckout: boolean;
  loginViaPhone: boolean;
}
interface IEmail {
  contactUsEmail: string;
  contactUsEmailBcc: string;
  salesEmailBcc: string;
}
interface ICurrency {
  isBaseCurrency: boolean;
  identifier: string; // !!!!! Displayed as selection
  mark: string;
  rate: number;
}
type TLang = 'Chinese' | 'English';

interface ILang {
  supportedLang: TLang[];
  mainLang: TLang;
}

interface IFBSetting {
  enabled: boolean;
  fbClientId?: string;
  fbClientSecret?: string;
}

interface ITemplate {
  templateType: 'default' | 'custom'; // !!!!! Displayed as selection
  supportedTemplates: {
    primaryColor: string;
    secondColor: string;
  };
  adminUIVersion: string; // !!!!! Displayed as selection
}

interface IGoogleRecap {
  websiteKey: string;
  enableRecapForm: string[]; // !!!!! Displayed as selection
}
interface ICommission {
  saveCodeInCustomerData: boolean;
  overrideSalesCode: boolean;
  decimalPlaces: number;
}
interface IAzure {
  apiUrl: string;
  apiKey: string;
}
interface IEmployee {
  enabled: boolean;
  startWorkAt: string; // !!!!! Displayed as selection
  offWorkAt: string; // !!!!! Displayed as selection
  lunchBreakFrom: string; // !!!!! Displayed as selection
  lunchBreakTo: string; // !!!!! Displayed as selection
  workHoursPerDay: number;
}
interface IClickUp {
  clientId: string;
  clientSecret: string;
  code?: string;
  token?: string;
}
interface IWhatsapp {
  wtspBAccId: string;
  phoneNumberId: string;
  accessToken: string;
}
interface ISms {
  username: string;
  pw: string;
  senderId: string;
}
interface IZoom {
  enabled: boolean;
  accountId: string;
  clientId: string;
  clientSecret: string;
}
