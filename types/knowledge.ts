import { IFileBasic } from "./file";


export interface IKnowledge{
    id: string;
    title: string;
    content: string;
    files?: IFileBasic[];
}