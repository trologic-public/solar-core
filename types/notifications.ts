export interface INotification {
  id: string;
  avatarUrl: string | null;
  type: string;
  category: string;
  isUnRead: boolean;
  createdAt: Date;
  title: string;
}
