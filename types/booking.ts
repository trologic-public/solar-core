import { IAddressItem } from "./address";
import { ICustomer } from "./customer";
import { IOrderLoyaltyPoint_Log } from "./loyalty-point";
import { IMembershipTier } from "./membership";
import { IOrderItem } from "./order";
import { IPayment } from "./payment";
import { IProductItem, IQuestionnaire } from "./product";
import { IPromotionItem } from "./promotion";
import { IAdmin } from "./user";

export interface IBookingCategory {
	id: string;
	enable: boolean;
	title: string;
	description: string;
	url: string;
	order: number;
	meta: {
		title?: string;
		description?: string;
	} | null;
	image: { src: string; alt: string; width?: number; height?: number } | null;
}

export enum EBookingStatus {
	unpaid = "unpaid",
	confirmed = "confirmed",
	unpaidCxl = "unpaid-cancelled",
	paidCxl = "paid-cancelled",
	refunded = "refunded",
}

export type IBookingCustomer = Pick<ICustomer, "id" | "displayName" | "phoneNumber" | "email" | "gender" | "tags" | "photoURL" | "membershipLvl">;

export interface IBooking {
	id: string;
	customer: IBookingCustomer
	product: IProductItem;
	status: EBookingStatus;
	start: Date;
	end: Date;
	duration?: number;
	description: string;
	createdAt: Date;
	updatedAt: Date;
	promotion?: IPromotionItem | null;
	orderRef?: IOrderItem | null;
	// updated types
	subTotal: number;
	totalAmount: number;
	remark?: string;
	membership?: IMembershipTier | null;
	questions?: IQuestionnaire | null;
	assignStaff?: IAdmin | null;
	vat?: number;
	billing?: IAddressItem | null;
	loyaltyPointLog?: IOrderLoyaltyPoint_Log;
	payments?: IPayment[];
}

// types for booking with slots
export interface IBookingTimeSlotOption {
	start: Date;
	end: Date;
	slots: number;
}

export type IBookingFilters = {
	customerName: string;
	phone: string;
	status: EBookingStatus | "all";
	productName: string;
	start: Date | null;
	end: Date | null;
};

export type IBookingTableFilterValue = string | EBookingStatus | "all" | Date | null;
