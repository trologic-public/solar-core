import { IImage, MaterialUIColorName } from "./general";

export enum IPageType {
	"Home" = "home",
	"About Us" = "about",
	"Contact Us" = "contact",
	"Shop" = "shop",
	"Booking" = "booking",
	"Post" = "post",
	"FAQ" = "faq",
	"Terms & Conditions" = "tnc",
	"Privacy Policy" = "pp",
	"Onboarding Enquiry" = "onboarding",
	"Course" = "course"
}

export enum ISectionAlignment {
	Left = "start",
	Center = "center",
	Right = "end",
}
export enum ISectionView {
	Carousel = "carousel",
	Grid = "grid",
	Banner = "banner",
}

export interface IImageItemFields {
	title?: string;
	subtitle?: string;
	url?: string;
}

export interface IImageItemPayload extends IImageItemFields {
	file: File | null;
}

export interface IImageItem extends IImageItemFields {
	image: IImage | null;
}

export interface ISection {
	name: string;
	alignment: ISectionAlignment;
	view: ISectionView;
	order: number;
	images: IImageItem[];
	text: string;
	youtubeEmbedLink?: string;
	// for carousel only - autoplay
	autoplay?: number | undefined | null;
	// for grid view - pagination item per page
	itemsPerLoad?: number | undefined | null;
}

export type SectionImgType = "imgUpload" | "YouTubeEmbed";

export interface IHeroCTA {
	name: string;
	url: string;
	target: "_blank" | "_self";
	color: MaterialUIColorName;
}
export interface IHero {
	backgroundImg?: IImage | null;
	backgroundOverlayOpacity: number;
	title: string;
	subtitle: string;
	CTAs: IHeroCTA[];
	enable: boolean;
}

export interface IPage {
	type: IPageType;
	title: string;
	description: string;
	path: string;
	isRoot: boolean;
	optional: boolean;
	customisable: boolean;
	enable: boolean;
	requiredLogin?: boolean;
	hero?: IHero;
	sections: ISection[];
	order: number;
}

export interface IStoreFrontSettings{
	settings: {
		pages: IPage[]
	}
}