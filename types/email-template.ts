import { T_MC_Email_Run, T_MC_MailingItem } from "./marketing-campaign";

export type TEmailTemplate = {
    id: string;
    name: string;
    createdAt: Date;
    updatedAt: Date;
    subject: string;
    content: string;
    attachments?: any[];
}

export type T_Send_Template_Props = {
    tenant_id: string;
    templateId: string;
    mailingList: T_MC_MailingItem[];
    from: string;
    campaignRun?: T_MC_Email_Run;
}