
// settings
export type ILoyaltyPoint_Usage_Settings = {
    earningRate: number;
    redeemRate: number;
    maxUsagePerOrder?: number | null;
}

export type ILoyaltyPoint_Content_Settings = {
    pointUnit: string;
    pointUnitPlural: string;
    pointName: string;
}

export type ILoyaltyPoint_Membership_Rate_Settings = (ILoyaltyPoint_Usage_Settings & {
    membershipId: string;
})

export interface ILoyaltyPoint_Global_Settings extends ILoyaltyPoint_Usage_Settings {
    enabled: boolean;
    memberOnly: boolean;
    memberships: ILoyaltyPoint_Membership_Rate_Settings[];
    content: ILoyaltyPoint_Content_Settings
}

// wallet that live under customer data
export interface ILoyaltyPoint_Wallet {
    earned: number;
    redeemed: number;
    currentBalance: number;
}

// log live uder order and booking
export interface IOrderLoyaltyPoint_Log {
    earning: number;
    redeeming: number;
}