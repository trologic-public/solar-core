// ----------------------------------------------------------------------

export type IPaymentCard = {
  id: string;
  cardNumber: string;
  cardType: string;
  primary?: boolean;
};

export enum E_Payment_Status {
  "Pending"="Pending",
  "Completed"="Completed",
  "Voided"="Voided",
  "Refunded"="Refunded"
}

export enum E_Payment_Gateway {
	"stripe"='stripe',
	'paypal'='paypal'
}

export interface IPayment {
	paymentId: string;
	paymentGateway?: E_Payment_Gateway;
	paymentMethod: string; // Stripe, PayPal, etc.
	paymentStatus: string; // Pending, Success, Failed, Refunded, etc.
	paymentAmount: number;
	paymentDetails?: any | null | undefined; // Assuming you're using Stripe
	transactionDate: Date;
}

export type T_Payment = IPayment;