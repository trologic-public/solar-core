export type TEncryptedData = {
    iv: string;
    content: string;
};

export type T_Secret_Doc_Base = {
    // apiKey: string;
}

export type T_Secret_Doc_Encrypted = T_Secret_Doc_Base & {
    stripe?: TEncryptedData;
    paypal?: TEncryptedData;
}

export type T_Secret_Doc_Decrypted = T_Secret_Doc_Base & {
    stripe?: string;
    paypal?: string;
}

export const defaultSecrets: T_Secret_Doc_Decrypted = {
    // apiKey: "",
    stripe: "",
    paypal: ""
}