export interface IContactUs {
	id: string;
	name: string;
	email: string;
	phone: string;
	subject: string;
	message: string;
	createdAt: Date;
	read: boolean;
}

export const emptyContactUs: IContactUs = {
	id: "new",
	name: "",
	email: "",
	phone: "",
	subject: "",
	message: "",
	createdAt: new Date(),
	read: false,
};
