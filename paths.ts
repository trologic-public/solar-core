import { IOrganization } from './types';
import { EUserPermissionLevel, IPathPermission, IUserRole } from './types/roles';
import { pathsPermissions } from './pathPermissions';

export const DASHBOARD = 'dashboard';
export const MAGIC = `magic`;

export const ROOTS = {
	AUTH: '/auth',
	DASHBOARD: `/${DASHBOARD}`,
	MAGIC: `/${MAGIC}`,
};

export const paths = {
	root: '/',
	comingSoon: '/coming-soon',
	maintenance: '/maintenance',
	pricing: '/pricing',
	payment: '/payment',
	about: '/about-us',
	contact: '/contact-us',
	terms: '/terms-and-conditions',
	pp: '/privacy-policy',
	faqs: '/faqs',
	page403: '/403',
	page404: '/404',
	page500: '/500',
	components: '/components',
	docs: 'https://docs.minimals.cc',
	changelog: 'https://docs.minimals.cc/changelog',
	zoneUI: 'https://mui.com/store/items/zone-landing-page/',
	minimalUI: 'https://mui.com/store/items/minimal-dashboard/',
	freeUI: 'https://mui.com/store/items/minimal-dashboard-free/',
	figma:
		'https://www.figma.com/file/kAYnYYdib0aQPNKZpgJT6J/%5BPreview%5D-Minimal-Web.v5.0.0?type=design&node-id=0%3A1&t=Al4jScQq97Aly0Mn-1',
	product: {
		root: `/shop`,
		categoryRoot: `/shop/category`,
		checkout: `/shop/checkout`,
		details: (id: string) => `/shop/${id}`,
	},
	booking: {
		root: `/booking`,
		categoryRoot: `/booking/category`,
		checkout: `/booking/checkout`,
		details: (id: string) => `/booking/${id}`,
		confirmation: (id: string) => `/booking/${id}/confirmation`,
	},
	course: {
		root: `/course`,
		details: (id: string) => `/course/${id}/details`,
		"new-enroll": (cid: string, eid: string) => `/course/course-schedule/${cid}/new-enrollment/${eid}`,
		"my-courses": "/course/my-courses",
		enrollment: (eid: string) => `/course/my-courses/enrollment/${eid}`,
	},
	order: {
		root: `/order`,
		// order completed is for order only, booking complete should goes to booking/comfirmation
		completed: (id: string) => `/order/${id}/completed`,
	},
	post: {
		root: `/post`,
		details: (slug: string) => `/post/${slug}`,
	},
	// AUTH
	auth: {
		// admin auth
		admin: {
			login: `${ROOTS.AUTH}/admin/login`,
			verify: `${ROOTS.AUTH}/admin/verify`,
			register: `${ROOTS.AUTH}/admin/register`,
			forgotPassword: `${ROOTS.AUTH}/admin/forgot-password`,
		},
		// customer auth (firebase)
		customer: {
			login: `${ROOTS.AUTH}/customer/login`,
			verify: `${ROOTS.AUTH}/customer/verify`,
			register: `${ROOTS.AUTH}/customer/register`,
			welcome: `${ROOTS.AUTH}/customer/welcome`,
			forgotPassword: `${ROOTS.AUTH}/customer/forgot-password`,
			fa2: {
				root: `${ROOTS.AUTH}/customer/fa2`,
				verify: `${ROOTS.AUTH}/customer/fa2/verify`,
			},
		},
	},
	account: {
		root: `/account`,
		order: `/account/order`,
		orderDetails: (id: string) => `/account/order/${id}`,
	},
	// Onboarding
	onboarding: {
		root: `/onboarding`,
		applications: {
			root: `/onboarding/applications`,
			new: `/onboarding/applications/new`
		},
	},

	// DASHBOARD
	dashboard: {
		// OVERVIEW
		root: ROOTS.DASHBOARD,
		calendar: `${ROOTS.DASHBOARD}/calendar`,
		kanban: `${ROOTS.DASHBOARD}/kanban`,
		staffChat: `${ROOTS.DASHBOARD}/staff-chat`,
		knowledge: {
			root: `${ROOTS.DASHBOARD}/knowledge`,
			edit: `${ROOTS.DASHBOARD}/knowledge/edit`,
		},
		reports: {
			root: `${ROOTS.DASHBOARD}/reports`,
			sales: `${ROOTS.DASHBOARD}/reports/sales`,
			productSales: `${ROOTS.DASHBOARD}/reports/product-sales`,
			customer: `${ROOTS.DASHBOARD}/reports/customer`,
			inventories: `${ROOTS.DASHBOARD}/reports/inventories`,
			purchase: `${ROOTS.DASHBOARD}/reports/purchase`,
			customerTotalSale: `${ROOTS.DASHBOARD}/reports/customer-total-sales`,
			commission: `${ROOTS.DASHBOARD}/reports/commission`,
			adminBookings: `${ROOTS.DASHBOARD}/reports/admin-bookings`,
			'broadcast-report': `${ROOTS.DASHBOARD}/reports/broadcast-report`,
		},

		// WEBSITE
		cms: {
			root: `${ROOTS.DASHBOARD}/cms`,
		},
		post: {
			root: `${ROOTS.DASHBOARD}/post`,
			new: `${ROOTS.DASHBOARD}/post/new`,
			details: (title: string) => `${ROOTS.DASHBOARD}/post/${title}`,
			edit: (title: string) => `${ROOTS.DASHBOARD}/post/${title}/edit`,
			categories: `${ROOTS.DASHBOARD}/post/categories`,
		},
		category: `${ROOTS.DASHBOARD}/category`,
		tag: {
			root: `${ROOTS.DASHBOARD}/tag`,
			new: `${ROOTS.DASHBOARD}/tag/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/tag/${id}`,
		},
		announcement: {
			root: `${ROOTS.DASHBOARD}/announcement`,
			new: `${ROOTS.DASHBOARD}/announcement/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/announcement/${id}/edit`,
		},

		// COMMERCE
		order: {
			root: `${ROOTS.DASHBOARD}/order`,
			details: (id: string) => `${ROOTS.DASHBOARD}/order/${id}`,
			new: `${ROOTS.DASHBOARD}/order/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/order/${id}/edit`,
		},
		booking: {
			root: `${ROOTS.DASHBOARD}/booking`,
			record: `${ROOTS.DASHBOARD}/booking/record`,
			new: `${ROOTS.DASHBOARD}/booking/new`,
			details: (id: string) => `${ROOTS.DASHBOARD}/booking/${id}`,
			category: {
				root: `${ROOTS.DASHBOARD}/booking/category`,
				new: `${ROOTS.DASHBOARD}/booking/category/new`,
				edit: (id: string) => `${ROOTS.DASHBOARD}/booking/category/${id}/edit`,
			},
		},
		product: {
			root: `${ROOTS.DASHBOARD}/product`,
			new: `${ROOTS.DASHBOARD}/product/new`,
			category: `${ROOTS.DASHBOARD}/product/category`,
			details: (id: string) => `${ROOTS.DASHBOARD}/product/${id}`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/product/${id}/edit`,
			slots: (id: string) => `${ROOTS.DASHBOARD}/product/${id}/slots`,
		},
		service: {
			root: `${ROOTS.DASHBOARD}/service`,
			new: `${ROOTS.DASHBOARD}/service/new`,
			category: `${ROOTS.DASHBOARD}/service/category`,
			details: (id: string) => `${ROOTS.DASHBOARD}/service/${id}`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/service/${id}/edit`,
			slots: (id: string) => `${ROOTS.DASHBOARD}/service/${id}/slots`,
		},

		// CONSIGNMENTS
		consignments: {
			root: `${ROOTS.DASHBOARD}/consignments`,
			consignee: {
				root: `${ROOTS.DASHBOARD}/consignments/consignee`,
				// new: `${ROOTS.DASHBOARD}/consignments/new-consignee`,
				details: (id: string) => `${ROOTS.DASHBOARD}/consignments/consignee/${id}`,
				stocks: {
					root: (id: string) => `${ROOTS.DASHBOARD}/consignments/consignee/${id}/stock`,
					details: (cid: string, id: string) => `${ROOTS.DASHBOARD}/consignments/consignee/${cid}/stock/${id}`
				},
				invoices: {
					root: (id: string) => `${ROOTS.DASHBOARD}/consignments/consignee/${id}/invoice`,
					details: (cid: string, id: string) => `${ROOTS.DASHBOARD}/consignments/consignee/${cid}/invoice/${id}`
				},
			}
		},

		// Courses
		"course-preset": {
			root: `${ROOTS.DASHBOARD}/course-preset`
		},
		course: {
			root: `${ROOTS.DASHBOARD}/course`,
			enrollment: (id: string) => `${ROOTS.DASHBOARD}/course/${id}/enrollment`
		},
		venue: {
			root: `${ROOTS.DASHBOARD}/venue`
		},
		"enrollment-refund-request": {
			root: `${ROOTS.DASHBOARD}/enrollment-refund-request`
		},

		// SUPPLY-CHAIN
		courier: {
			root: `${ROOTS.DASHBOARD}/courier`,
			new: `${ROOTS.DASHBOARD}/courier/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/courier/${id}/edit`,
		},
		inventory: {
			root: `${ROOTS.DASHBOARD}/inventory`,
			warehouse: {
				root: `${ROOTS.DASHBOARD}/inventory/warehouse`,
				new: `${ROOTS.DASHBOARD}/inventory/warehouse/new`,
				details: (id: string) => `${ROOTS.DASHBOARD}/inventory/warehouse/${id}/edit`,
			},
		},
		supplier: {
			root: `${ROOTS.DASHBOARD}/supplier`,
			new: `${ROOTS.DASHBOARD}/supplier/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/supplier/${id}/edit`,
		},
		'purchase-order': {
			root: `${ROOTS.DASHBOARD}/purchase-order`,
			new: `${ROOTS.DASHBOARD}/purchase-order/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/purchase-order/${id}/edit`,
		},

		// MARKETING
		'email-template': {
			root: `${ROOTS.DASHBOARD}/email-template`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/email-template/${id}`,
			new: `${ROOTS.DASHBOARD}/email-template/new`,
		},
		campaign: {
			root: `${ROOTS.DASHBOARD}/campaign`,
			details: (id: string) => `${ROOTS.DASHBOARD}/campaign/${id}`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/campaign/${id}/edit`,
			runs: (id: string) => `${ROOTS.DASHBOARD}/campaign/${id}/runs`,
			new: (id: string) => `${ROOTS.DASHBOARD}/campaign/new`,
		},
		promotion: {
			root: `${ROOTS.DASHBOARD}/promotion`,
			new: `${ROOTS.DASHBOARD}/promotion/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/promotion/${id}/edit`,
		},
		conversion: {
			root: `${ROOTS.DASHBOARD}/conversion`,
		},
		funnel: {
			root: `${ROOTS.DASHBOARD}/funnel`,
			new: `${ROOTS.DASHBOARD}/funnel/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/funnel/${id}/edit`,
		},
		'sales-funnel': {
			root: `${ROOTS.DASHBOARD}/sales-funnel`,
			default: `${ROOTS.DASHBOARD}/sales-funnel/default`,
			custom: (id: string) => `${ROOTS.DASHBOARD}/sales-funnel/custom/${id}`,
		},
		automations: {
			root: `${ROOTS.DASHBOARD}/automations`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/automations/${id}`,
		},
		broadcast: {
			root: `${ROOTS.DASHBOARD}/broadcast`,
			new: `${ROOTS.DASHBOARD}/broadcast/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/broadcast/${id}/edit`,
			history: `${ROOTS.DASHBOARD}/broadcast/history`,
		},
		onboarding: {
			root: `${ROOTS.DASHBOARD}/onboarding`,
			settings: `${ROOTS.DASHBOARD}/onboarding/settings`,
			details: (id: string) => `${ROOTS.DASHBOARD}/onboarding/${id}`,
		},
		form: {
			root: `${ROOTS.DASHBOARD}/form`,
			new: `${ROOTS.DASHBOARD}/form/new`,
		},

		// CUSTOMER
		customer: {
			root: `${ROOTS.DASHBOARD}/customer`,
			new: `${ROOTS.DASHBOARD}/customer/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/customer/${id}/edit`,
			blacklist: `${ROOTS.DASHBOARD}/customer/blacklist`,
		},
		membership: {
			root: `${ROOTS.DASHBOARD}/membership`,
			new: `${ROOTS.DASHBOARD}/membership/new`,
			details: (id: string) => `${ROOTS.DASHBOARD}/membership/${id}/edit`,
		},
		'loyalty-program': {
			root: `${ROOTS.DASHBOARD}/loyalty-program`,
		},
		enquiry: {
			root: `${ROOTS.DASHBOARD}/enquiry`,
			details: (id: string) => `${ROOTS.DASHBOARD}/enquiry/${id}`,
		},

		// BACK OFFICE
		staff: {
			root: `${ROOTS.DASHBOARD}/staff`,
			new: `${ROOTS.DASHBOARD}/staff/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/staff/${id}/edit`,
			invitation: {
				root: `${ROOTS.DASHBOARD}/staff/invitation`,
				invite: `${ROOTS.DASHBOARD}/staff/invitation/invite`,
			}
		},
		'ai-settings': { root: `${ROOTS.DASHBOARD}/ai-settings` },
		'staff-role': { 
			root: `${ROOTS.DASHBOARD}/staff-role`,
			new: `${ROOTS.DASHBOARD}/staff-role/new`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/staff-role/${id}`
		},
		'staff-logs': { root: `${ROOTS.DASHBOARD}/staff-logs` },
		'system-settings': {
			root: `${ROOTS.DASHBOARD}/system-settings`,
			// Super Only
			'module-settings': `${ROOTS.DASHBOARD}/system-settings/module-settings`,
			'custom-locales': `${ROOTS.DASHBOARD}/system-settings/custom-locales`,
		},

		// finance
		finance: {
			root: `${ROOTS.DASHBOARD}/finance`,
			transaction: `${ROOTS.DASHBOARD}/finance/transaction`,
			account: `${ROOTS.DASHBOARD}/finance/account`,
			settings: `${ROOTS.DASHBOARD}/finance/settings`,
			report: `${ROOTS.DASHBOARD}/finance/report`,
			logs: `${ROOTS.DASHBOARD}/finance/logs`,
		},

		// Unused
		admin: {
			root: `${ROOTS.DASHBOARD}/admin`,
			invitations: `${ROOTS.DASHBOARD}/admin/invitations`,
		},
		mail: `${ROOTS.DASHBOARD}/mail`,
		chat: `${ROOTS.DASHBOARD}/chat`,
		blank: `${ROOTS.DASHBOARD}/blank`,

		fileManager: `${ROOTS.DASHBOARD}/file-manager`,
		permission: `${ROOTS.DASHBOARD}/permission`,
		attribute: {
			root: `${ROOTS.DASHBOARD}/attribute`,
			new: `${ROOTS.DASHBOARD}/attribute/new`,
		},
		general: {
			app: `${ROOTS.DASHBOARD}/app`,
			ecommerce: `${ROOTS.DASHBOARD}/ecommerce`,
			analytics: `${ROOTS.DASHBOARD}/analytics`,
			banking: `${ROOTS.DASHBOARD}/banking`,
			booking: `${ROOTS.DASHBOARD}/booking`,
			file: `${ROOTS.DASHBOARD}/file`,
		},
		user: {
			root: `${ROOTS.DASHBOARD}/user`,
			new: `${ROOTS.DASHBOARD}/user/new`,
			list: `${ROOTS.DASHBOARD}/user/list`,
			cards: `${ROOTS.DASHBOARD}/user/cards`,
			profile: `${ROOTS.DASHBOARD}/user/profile`,
			account: `${ROOTS.DASHBOARD}/user/account`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/user/${id}/edit`,
		},
		invoice: {
			root: `${ROOTS.DASHBOARD}/invoice`,
			new: `${ROOTS.DASHBOARD}/invoice/new`,
			details: (id: string) => `${ROOTS.DASHBOARD}/invoice/${id}`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/invoice/${id}/edit`,
		},
		job: {
			root: `${ROOTS.DASHBOARD}/job`,
			new: `${ROOTS.DASHBOARD}/job/new`,
			details: (id: string) => `${ROOTS.DASHBOARD}/job/${id}`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/job/${id}/edit`,
		},
		tour: {
			root: `${ROOTS.DASHBOARD}/tour`,
			new: `${ROOTS.DASHBOARD}/tour/new`,
			details: (id: string) => `${ROOTS.DASHBOARD}/tour/${id}`,
			edit: (id: string) => `${ROOTS.DASHBOARD}/tour/${id}/edit`,
		},
	},

	// Magic Links sections
	magic: {
		admin: {
			invitation: (token: string) => `${ROOTS.MAGIC}/admin/invitation/${token}`,
			// TBC magic link to reset pw, etc
		},
		customer: {
			// TBC magic links for email link login, forgot/reset pw, email verify
		}
	}
};
interface Params {
    [key: string]: string | string[];
}
interface IPathPermissionCheckProps {
	role?: IUserRole;
	path: string;
	organization: IOrganization;
	routeParams: Params;
}
export const checkPathPermission = ({
	role,
	path,
	organization,
	routeParams,
}: IPathPermissionCheckProps) => {
	// console.log('checkPathPermission path', path);
	const { tenantId } = organization;
	const params = Object.values(routeParams);

	const pathKeys = path
		.trim()
		.split('/')
		.filter((part) => !['', tenantId, DASHBOARD, ...params].includes(part))
		.join('.');
	// console.log('path keys', pathKeys);

	const pathPermission: IPathPermission | undefined = pathsPermissions.find(
		(item) => item.path === pathKeys
	);

	if (!pathPermission) {
		// console.log('no path permission found, allow access');
		return true;
	}
	// console.log('pathPermission', pathPermission);

	const roleScopedPermission =
		role?.permissions.find((p) => p.scope === pathPermission.permission.scope)?.permission ??
		role?.defaultPermission ??
		EUserPermissionLevel['Can Read'];

	// console.log('roleScopedPermission', roleScopedPermission);

	if (roleScopedPermission >= pathPermission.permission.permission) {
		return true;
	}

	return false;
};
