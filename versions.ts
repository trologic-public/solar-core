import { ROOTS, paths } from "./paths";
import { DeepPartial } from "./types";

export const LTS_customPaths: DeepPartial<typeof paths> = {
    dashboard: {
        kanban: "/dashboard/tasks",
        funnel: {
            root: `${ROOTS.DASHBOARD}/sales-funnel`,
        },
    },
}