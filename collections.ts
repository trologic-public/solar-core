// define some constants first
export const collections = {
	/**
	 * Root collections are NOT under "organizations", 
	 * make sure when using it, do not use it with orgCollection helpers
	 */
	topRoot: {
		financeTransactions: 'finance-transactions',
		financeLogs: 'finance-logs',
		productMap: 'product-map',
		userSeeds: 'user-seeds',
		coursePresets: 'course-presets',
		courses: {
			root: 'courses',
			subCollections: {
				enrollments: 'enrollments'
			}
		},
		financeSummary: {
			root: 'finance-summary',
			monthCollection: "months",
			retainEarnings: 'retain-earnings',
		}
	},
	adminInvitations: `admin-invitations`,
	users: "users",
	global: "global-settings",
	moduleSettings: `module-settings`,
	products: "products",
	posts: "posts",
	onboardings: "onboardings",
	onboardingSettings: "onboarding-settings",
	bookings: "bookings",
	updatedBookings: "updated-bookings",
	tags: "tags",
	meetings: "meetings",
	contacts: "contacts",
	announcements: "announcements",
	broadcasts: "broadcasts",
	bookingCategories: "bookingCategories",
	promotions: "promotions",
	kanban: "kanban",
	warehouses: "warehouses",
	warehouseDefault: "warehouse-default",
	funnels: "funnels",
	salesFunnel: "sales-funnels",
	orders: "orders",
	productCategories: "productCategories",
	purchaseOrders: "purchaseOrders",
	staffLogs: "staffLogs",
	memberships: "memberships",
	postCategories: "postCategories",
	contactUs: "contact-us",
	couriers: "couriers",
	categories: "categories",
	organizations: "organizations",
	tasks: "tasks",
	customSystemLocales: "custom-system-locales",
	knowledgeDatabase: "knowledge-database",
	loyaltyProgram: "loyalty-program",
	otp: "one-time-password",
	conversions: "conversions",
	crmMissions: 'crm-missions',
	emailTemplates: 'email-templates',
	marketingCampaigns: 'marketing-campaigns',
	financeAccounts: 'finance-accounts',
	financeRecipients: 'finance-recipients',
	financeSettings: 'finance-settings',
	// customer-blacklist
	customerBlacklist: 'customer-blacklist',
	// secrets (this collection should only be accessible by firebase admin access)
	secrets: 'secrets',
	/**
	 * Consignment collections
	 */
	consignment: {
		consignees: {
			root: 'consignees',
			subCollections: {
				outlets: 'outlets',
				stocks: 'stocks',
				invoices: 'invoices'
			} 
		}
	},
	/**
	 * Reports V2 - Consolidated
	 */
	reports: {
		root: "reports",
		subCollections: {
			daily: "daily", monthly: "monthly"
		}
	},
	/**
	 * Staff roles
	 */
	customStaffRoles: `custom-staff-roles`,
	staff: {
		invitation: "staff-invitations"
	},
	// chat room
	chatroom: {
		root: "chatrooms",
		subCollections: {
			messages: "messages"
		}
	},
	// venues
	venues: {
		root: 'venues',
		subCollections: {
			maintenances: "maintenances"
		}
	},
};