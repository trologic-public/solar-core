import { cloneDeep } from "lodash";
import { TTrackingData, TTrackingEventRecord, TTrackingSourceRecord } from "../../types/tracking";


export const defaultData: TTrackingData = {
    uids: [] as string[],
    count: 0,
    salesValue: 0
}

export const defaultEventsRecord: TTrackingEventRecord = {
    "Landing": cloneDeep(defaultData),
    "Sign Up": cloneDeep(defaultData),
    "Checkout": cloneDeep(defaultData),
    "Order": cloneDeep(defaultData)
}

export const defaultSourceRecord: TTrackingSourceRecord = {
    "Direct": cloneDeep(defaultEventsRecord),
    "Google Ad": cloneDeep(defaultEventsRecord),
    "FB Pixel": cloneDeep(defaultEventsRecord),
    "Email": cloneDeep(defaultEventsRecord),
    "Social Media Ads": cloneDeep(defaultEventsRecord),
    "Search Engine Marketing": cloneDeep(defaultEventsRecord),
    "Others": cloneDeep(defaultEventsRecord),
}