import { E_MC_Status, TMarketingCampaign } from "../../types/marketing-campaign";


export const defaultMarketingCampaign: TMarketingCampaign = {
    id: "",
    name: "",
    description: "",
    status: E_MC_Status.pending,
    targets: [],
    trackingSources: [],
    defaultMailingList: [],
}