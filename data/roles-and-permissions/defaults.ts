import { paths } from "../../paths";
import { EUserPermissionLevel, IUserPermission, IUserRole } from "../../types/roles";

interface IGenPermissionProps {
    defaultPermission: EUserPermissionLevel;
    override?: IUserPermission[]
}

export const genPermissions = ({
    defaultPermission,
    override = []
}: IGenPermissionProps) => {
    const allPermissions = Object.keys(paths.dashboard).map(scope => ({
        scope,
        permission: override.find(oo => oo.scope === scope)?.permission ?? defaultPermission
    } as IUserPermission))

    return allPermissions;
}

export enum E_Admin_Role_Id {
    "super-admin" = "super-admin",
    'admin' = 'admin',
    'finance' = 'finance',
    'sales' = 'sales',
    'website-operator' = 'website-operator',
    'business-operator' = 'business-operator',
    'warehouse-operator' = 'warehouse-operator'
}

// Solar super admins
export const SuperAdminRole: IUserRole = {
    id: E_Admin_Role_Id["super-admin"],
    name: "Super Administrator",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: true,
    defaultPermission: EUserPermissionLevel.Super,
    permissions: genPermissions({ defaultPermission: EUserPermissionLevel.Super })
}

export const AdministratorRole: IUserRole = {
    id: E_Admin_Role_Id.admin,
    name: "Administrator",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: false,
    defaultPermission: EUserPermissionLevel["Can Edit"],
    permissions: genPermissions({ defaultPermission: EUserPermissionLevel["Can Edit"] })
}

export const FinanceRole: IUserRole = {
    id: E_Admin_Role_Id.finance,
    name: "Finance",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: false,
    defaultPermission: EUserPermissionLevel["Can Read"],
    permissions: genPermissions({
        defaultPermission: EUserPermissionLevel["Can Read"],
        override: [
            // overview
            { scope: "calendar", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "kanban", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "knowledge", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "reports", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "finance", permission: EUserPermissionLevel["Can Edit"] }
        ]
    })
}

export const SalesRole: IUserRole = {
    id: E_Admin_Role_Id.sales,
    name: "Sales",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: false,
    defaultPermission: EUserPermissionLevel["Can Read"],
    permissions: genPermissions({
        defaultPermission: EUserPermissionLevel["Can Read"],
        override: [
            // overview
            { scope: "calendar", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "kanban", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "knowledge", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "reports", permission: EUserPermissionLevel["Can Edit"] },
            // commerce
            { scope: "order", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "booking", permission: EUserPermissionLevel["Can Edit"] },
            // Customer
            { scope: "customer", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "membership", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "loyalty-program", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "enquiry", permission: EUserPermissionLevel["Can Edit"] },
            // CRM
            { scope: "funnel", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "sales-funnel", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "broadcast", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "promotion", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "onboarding", permission: EUserPermissionLevel["Can Edit"] },
        ]
    })
}

export const WebsiteOperatorRole: IUserRole = {
    id: E_Admin_Role_Id["website-operator"],
    name: "Website Operator",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: false,
    defaultPermission: EUserPermissionLevel["Can Read"],
    permissions: genPermissions({
        defaultPermission: EUserPermissionLevel["Can Read"],
        override: [
            // overview
            { scope: "calendar", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "kanban", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "knowledge", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "reports", permission: EUserPermissionLevel["Can Edit"] },
            // CMS
            { scope: "cms", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "post", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "category", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "tag", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "announcement", permission: EUserPermissionLevel["Can Edit"] },
        ]
    })
}

export const BusinessOperatorRole: IUserRole = {
    id: 'business-operator',
    name: "Business Operator",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: false,
    defaultPermission: EUserPermissionLevel["Can Read"],
    permissions: genPermissions({
        defaultPermission: EUserPermissionLevel["Can Read"],
        override: [
            // overview
            { scope: "calendar", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "kanban", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "knowledge", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "reports", permission: EUserPermissionLevel["Can Edit"] },
            // CMS
            { scope: "category", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "tag", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "announcement", permission: EUserPermissionLevel["Can Edit"] },
            // Commerce
            { scope: "product", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "service", permission: EUserPermissionLevel["Can Edit"] },
            // Supply chain
            { scope: "courier", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "inventory", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "supplier", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "purchase-order", permission: EUserPermissionLevel["Can Edit"] },
            // Customer
            { scope: "customer", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "enquiry", permission: EUserPermissionLevel["Can Edit"] },
        ]
    })
}

export const WarehouseOperatorRole: IUserRole = {
    id: 'warehouse-operator',
    name: "Warehouse Operator",
    external: false,
    isDefault: false,
    systemDefault: true,
    allowDelete: false,
    allowEdit: false,
    hidden: false,
    defaultPermission: EUserPermissionLevel["Can Read"],
    permissions: genPermissions({
        defaultPermission: EUserPermissionLevel["Can Read"],
        override: [
            // overview
            { scope: "calendar", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "kanban", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "knowledge", permission: EUserPermissionLevel["Can Edit"] },
            { scope: "reports", permission: EUserPermissionLevel["Can Edit"] },
            // warehouse
            { scope: "inventory", permission: EUserPermissionLevel["Can Edit"]  },
            { scope: "purchase-order", permission: EUserPermissionLevel["Can Edit"]  },
        ]
    })
}

export const AllSystemUserRoles: IUserRole[] = [
    SuperAdminRole,
    AdministratorRole,
    BusinessOperatorRole,
    FinanceRole,
    SalesRole,
    WebsiteOperatorRole,
    WarehouseOperatorRole
]