import { ILoyaltyPoint_Wallet } from "../../types/loyalty-point";


export const defaultWallet: ILoyaltyPoint_Wallet = {
    earned: 0,
    redeemed: 0,
    currentBalance: 0
}