import { IKanban2_Data_Board } from "../../types/kanban-v2";


export const defaultBoardData: IKanban2_Data_Board = {
    id: 'default',
    name: 'Default Funnel',
    createdAt: new Date(),
    isDefault: true,
    disabled: false,
    canDelete: false,
    readOnly: false,
    cardDetailsDisplay: 'drawer',
    enableAutomation: true
};

export const newBoardData: IKanban2_Data_Board = {
    id: 'new',
    name: 'New Funnel',
    createdAt: new Date(),
    isDefault: false,
    disabled: false,
    canDelete: true,
    readOnly: false,
    cardDetailsDisplay: 'drawer',
    enableAutomation: true
};