import { paths } from "./paths";
import { NestedKeys } from "./types";
import { EUserPermissionLevel, IPathPermission } from "./types/roles";

const editAccessPaths: {
    path: NestedKeys<typeof paths.dashboard>, 
    scope: keyof typeof paths.dashboard,
    superOnly?: boolean
}[] = [
    // Overview
    // Website
    {path: "cms", scope: "cms"},
    {path: "category", scope: "category"},
    {path: "tag.new", scope: "tag"},
    {path: "tag.edit", scope: "tag"},
    {path: "post.new", scope: "post"},
    {path: "post.edit", scope: "post"},
    {path: "announcement.new", scope: "announcement"},
    {path: "announcement.edit", scope: "announcement"},
    // Commerce
    {path: "order.edit", scope: "order"},
    {path: "order.new", scope: "order"},
    {path: "product.edit", scope: "product"},
    {path: "product.new", scope: "product"},
    {path: "product.slots", scope: "product"},
    {path: "service.new", scope: "service"},
    {path: "service.edit", scope: "service"},
    {path: "service.slots", scope: "service"},
    {path: "consignments.consignee", scope: "consignments"},
    // Supply Chain
    {path: "courier.new", scope: "courier"},
    {path: "courier.edit", scope: "courier"},
    {path: "inventory.warehouse.new", scope: "inventory"},
    {path: "purchase-order.new", scope: "purchase-order"},
    {path: "purchase-order.edit", scope: "purchase-order"},
    // Marketing
    {path: "promotion.new", scope: "promotion"},
    {path: "promotion.edit", scope: "promotion"},
    {path: "funnel.new", scope: "funnel"},
    {path: "funnel.edit", scope: "funnel"},
    {path: "broadcast.new", scope: "broadcast"},
    {path: "broadcast.edit", scope: "broadcast"},
    {path: "sales-funnel", scope: "sales-funnel"},
    {path: "onboarding", scope: "onboarding"},
    // Customer
    {path: "customer.new", scope: "customer"},
    {path: "customer.edit", scope: "customer"},
    {path: "membership.new", scope: "membership"},
    {path: "loyalty-program", scope: "loyalty-program"},
    {path: "enquiry", scope: "enquiry"},
    // Back office
    {path: "staff", scope: "staff"},
    {path: "system-settings", scope: "system-settings"},
    {path: "staff-role", scope: "staff-role"},
    {path: "staff-logs", scope: "staff-logs"},
    // finance
    {path: "finance", scope: "finance"},
    // Super only
    {path: "system-settings.module-settings", scope: "system-settings", superOnly: true},
    {path: "system-settings.custom-locales", scope: "system-settings", superOnly: true}
]

// map of paths that needs permissions
export const pathsPermissions: IPathPermission[] = editAccessPaths.map(item => ({
    path: item.path,
    permission: {
        scope: item.scope,
        permission: item.superOnly ? EUserPermissionLevel.Super : EUserPermissionLevel["Can Edit"]
    }
}))